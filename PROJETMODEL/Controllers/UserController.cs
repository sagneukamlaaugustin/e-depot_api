﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NSwag.Annotations;
using NSwag.SwaggerGeneration.WebApi;
using PROJETMODEL.Models.DAL;
using PROJETMODEL.Models.VML.Param;

namespace PROJETMODEL.Controllers
{
    public class UserController : ODataController
    {
        private readonly Store db;
        /// <summary>
        /// Initialisation du contexte de donnée
        /// </summary>
        /// <param name="dbContext"></param>
        public UserController(Store dbContext) => this.db = dbContext;
        /// <summary>
        /// Retourne la liste de toutes les elements
        /// </summary>
        /// <returns></returns>
        /// 

        // GET: api/User
        [EnableQuery]
        [ProducesResponseType(typeof(List<TgUtilisateurs>), 200)]
        public IActionResult Get()
        {
            var Resultat = db.TgUtilisateurs.Where(p => p.Actif == true)
                                   .Select(p => new
                                   {
                                       p.CodeUser,
                                       p.IdPersonnelNavigation.Nom,
                                       p.IdPersonnelNavigation.Prenom,
                                       p.IdPersonnelNavigation.Actif,
                                       p.Login,
                                       p.SommeCtrl,
                                       p.IdPersonnelNavigation.Email,
                                       p.IdPersonnelNavigation.Telephone
                                   });
            return Ok(Resultat);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(TgUtilisateurs), 200)]
        [ProducesResponseType(typeof(void), 600)]
        [ProducesResponseType(typeof(void), 500)]
        public  IActionResult Post([FromBody] UserParam model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var Resultat = db.TgUtilisateurs.Where(p => p.Actif == true && p.Login == model.Login && p.SommeCtrl == model.PassWord)
                                  .Select(p => new
                                  {
                                      p.CodeUser,
                                      p.IdPersonnelNavigation.Nom,
                                      p.IdPersonnelNavigation.Prenom,
                                      p.IdPersonnelNavigation.Actif,
                                      p.Login,
                                      p.SommeCtrl,
                                      p.IdPersonnelNavigation.Email,
                                      p.IdPersonnelNavigation.Telephone
                                  });

                if(Resultat.Count() == 0)
                   return StatusCode(600); // user n'exixte pas 
                else return  Ok(Resultat);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }


        [SwaggerIgnore]
        [HttpGet("UserDoc")]
        public async Task<ContentResult> Swagger()
        {
            var settings = new WebApiToSwaggerGeneratorSettings
            {
                DefaultUrlTemplate = "api/{controller}/{action}/{id}",
            };
            var generator = new WebApiToSwaggerGenerator(settings);
            var document = await generator.GenerateForControllerAsync<UserController>();
            var swaggerSpecification = document.ToJson();
            return Content(document.ToJson());
        }
    }
}
