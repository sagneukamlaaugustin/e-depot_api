﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using NSwag.SwaggerGeneration.WebApi;
using PROJETMODEL.Models.DAL;

namespace PROJETMODEL.Controllers
{

    public class CategorieController : ODataController
    {
        private readonly Store db;
        /// <summary>
        /// Initialisation du contexte de donnée
        /// </summary>
        /// <param name="dbContext"></param>
        public CategorieController(Store dbContext) => this.db = dbContext;
        /// <summary>
        /// Retourne la liste de toutes les elements
        /// </summary>
        /// <returns></returns>
        /// 

        // GET: api/User
        [EnableQuery]
        [ProducesResponseType(typeof(List<TpProduit>), 200)]
        public IActionResult Get()
        {
            var Resultat = db.TpCategorie.Where(p => p.Actif == true)
                                   .Select(p => new
                                   {
                                       p.IdCategorie,
                                       p.Libelle
                                   });
            return Ok(Resultat);
        }

        [SwaggerIgnore]
        [HttpGet("CategorieDoc")]
        public async Task<ContentResult> Swagger()
        {
            var settings = new WebApiToSwaggerGeneratorSettings
            {
                DefaultUrlTemplate = "api/{controller}/{action}/{id}",
            };
            var generator = new WebApiToSwaggerGenerator(settings);
            var document = await generator.GenerateForControllerAsync<CategorieController>();
            var swaggerSpecification = document.ToJson();
            return Content(document.ToJson());
        }
    }
}
