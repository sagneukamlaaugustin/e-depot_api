﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NSwag.Annotations;
using NSwag.SwaggerGeneration.WebApi;
using PROJETMODEL.Models.DAL;
using PROJETMODEL.Models.VML;

namespace PROJETMODEL.Controllers
{
    ///
    /// produit api controller
    ///
   // [Authorize]
    public class ProduitController : ODataController
    {
        private readonly Store db;
        /// <summary>
        /// Initialisation du contexte de donnée
        /// </summary>
        /// <param name="dbContext"></param>
        public ProduitController(Store dbContext) => this.db = dbContext;
        /// <summary>
        /// Retourne la liste de toutes les elements
        /// </summary>
        /// <returns></returns>
        // GET: item
        [EnableQuery]
        [ProducesResponseType(typeof(List<TpProduit>), 200)]
        public IActionResult Get()
        {
            var Resultat = db.TpProduit.Where(p => p.Actif == true)
                                   .Select(p => new
                                   {
                                       p.IdProduit,
                                       p.IdCategorie,
                                       p.IdFamille,
                                       p.Libelle,
                                       p.Code,
                                       PuVente = p.PuVente / p.IdEmballageNavigation.Conditionnement,
                                       PuAchat = p.PuAchat / p.IdEmballageNavigation.Conditionnement,
                                       p.Image
                                   });
            return Ok(Resultat);
        }
        /// <summary>
        /// Retourne une seule vente 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        // GET: item(1)
        [EnableQuery]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(TpProduit), 200)]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        public async Task<IActionResult> Get([FromRoute] int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var model = await db.TpProduit.FindAsync(key);

            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        // PUT: item(1)
        /// <summary>
        /// Modifié un element
        /// </summary>
        /// <param name="key"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(NoContentResult),204)]
        public async Task<IActionResult> Put([FromRoute] int key, [FromBody] TpProduit model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != model.IdProduit)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return NoContent();
        }
        /// <summary>
        /// Ajoute un élement
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // POST: item
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NoContentResult),204)]
        public async Task<IActionResult> Post([FromBody] TpProduit model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

              db.Entry(model).State = EntityState.Added;
            await db.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Supprime un élement
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        /// <response code="400">La requette n'est pas comprises par le serveur</response>
        /// <response code="404">Client introuvable pour l'id specifié</response>
        /// <response code="200">Requette executé avec succes</response>
        // DELETE: item(1)
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(TpProduit), 200)]
        public async Task<IActionResult> Delete([FromRoute] int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var model = await db.TpProduit.FindAsync(key);
            if (model == null)
            {
                return NotFound();
            }

            db.Entry(model).State = EntityState.Deleted;

            await db.SaveChangesAsync();
            return Ok(model);
        }
        /// <summary>
        /// Verifie si une compagne existe
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool CompanyExists(int id)
        {
            return db.TpProduit.Any(e => e.IdProduit == id);
        }

        [SwaggerIgnore]
        [HttpGet("ProduitDoc")]
        public async Task<ContentResult> Swagger()
        {
            var settings = new WebApiToSwaggerGeneratorSettings
            {
                DefaultUrlTemplate = "api/{controller}/{action}/{id}",
            };
            var generator = new WebApiToSwaggerGenerator(settings);
            var document = await generator.GenerateForControllerAsync<ProduitController>();
            var swaggerSpecification = document.ToJson();
            return Content(document.ToJson());
        }
    }
}