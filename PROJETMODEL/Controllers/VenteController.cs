﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NSwag.Annotations;
using NSwag.SwaggerGeneration.WebApi;
using PROJETMODEL.Models.DAL;

namespace PROJETMODEL.Controllers
{
    ///
    /// vente api controller
    ///
   // [Authorize]
    public class VenteController : ODataController
    {
        private readonly Store db;
        /// <summary>
        /// Initialisation du contexte de donnée
        /// </summary>
        /// <param name="dbContext"></param>
        public VenteController(Store dbContext) => this.db = dbContext;
        /// <summary>
        /// Retourne la liste de toutes les vente mobiles
        /// </summary>
        /// <returns></returns>
        // GET: companies
        [EnableQuery]
        [ProducesResponseType(typeof(List<TpVente>), 200)]
        public IActionResult Get()
        {
            var Resultat =  db.TpVente
                            .Select(p => new
                            {
                                p.IdVente,
                                p.IdCaisse,
                                IdCaissiere = p.IdClient,
                                p.Etat,
                                p.Date,
                                p.NumDoc
                            });
            return Ok(Resultat);                             
        }
        /// <summary>
        /// Retourne une seule vente 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        // GET: companies(1)
        [EnableQuery]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(TpVente), 200)]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        public async Task<IActionResult> Get([FromRoute] int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var model = await db.TpVente.FindAsync(key);

            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        // PUT: companies(1)
        /// <summary>
        /// Modifie une vente
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(NoContentResult),204)]
        public async Task<IActionResult> Put([FromRoute] int key, [FromBody] TpVente model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != model.IdVente)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            foreach (var TpVenteLiquide in model.TpVenteLiquide)
            {
                db.Entry(TpVenteLiquide).State = EntityState.Modified;
            }

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return NoContent();
        }
        /// <summary>
        /// Ajoute une vente
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // POST: companies
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NoContentResult),204)]
        public async Task<IActionResult> Post([FromBody] TpVente model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
   
            db.Entry(model).State = EntityState.Added;

            foreach(var VenteMobileDetail in model.TpVenteLiquide)
            {
                db.Entry(VenteMobileDetail).State = EntityState.Added;
            }

            await db.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Supprime une vente
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        /// <response code="400">La requette n'est pas comprises par le serveur</response>
        /// <response code="404">Client introuvable pour l'id specifié</response>
        /// <response code="200">Requette executé avec succes</response>
        // DELETE: companies(1)
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(TpVente), 200)]
        public async Task<IActionResult> Delete([FromRoute] int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var model = await db.TpVente.FindAsync(key);
            if (model == null)
            {
                return NotFound();
            }

            db.Entry(model).State = EntityState.Deleted;

            await db.SaveChangesAsync();
            return Ok(model);
        }
        /// <summary>
        /// Verifie si une compagne existe
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected bool CompanyExists(int key)
        {
            return db.TpVente.Any(e => e.IdVente == key);
        }

        [SwaggerIgnore]
        [HttpGet("VenteDoc")]
        public async Task<ContentResult> Swagger()
        {
            var settings = new WebApiToSwaggerGeneratorSettings
            {
                DefaultUrlTemplate = "api/{controller}/{action}/{id}",
            };
            var generator = new WebApiToSwaggerGenerator(settings);
            var document = await generator.GenerateForControllerAsync<VenteController>();
            var swaggerSpecification = document.ToJson();
            return Content(document.ToJson());
        }
    }
}
