﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NSwag.Annotations;
using NSwag.SwaggerGeneration.WebApi;
using PROJETMODEL.Models.DAL;

namespace PROJETMODEL.Controllers
{
    ///
    /// vente detail api controller
    ///
    public class VenteDetailsController : ODataController
    {
    
        private readonly Store db;
        /// <summary>
        /// Initialisation du contexte de donnée
        /// </summary>
        /// <param name="dbContext"></param>
        public VenteDetailsController(Store dbContext) => this.db = dbContext;
        /// <summary>
        /// Retourne la liste de toutes les details de vente mobiles
        /// </summary>
        /// <returns></returns>
        // GET: companies
        [EnableQuery]
        [ProducesResponseType(typeof(List<TpVenteLiquide>), 200)]
        public IActionResult Get()
        {
            var Resultat = db.TpVenteLiquide
                           .Select(p => new
                           {
                               p.IdVente,
                               p.IdProduit,
                               p.QuantiteBouteilleLiquide,
                               PuAchat = p.PuAchat / p.Conditionnement,
                               PuVente = p.PuVente / p.Conditionnement
                           });

            return Ok(Resultat);
        }
        /// <summary>
        /// Retourne un detail vente 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        // GET: companies(1)
        [EnableQuery]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(TpVenteLiquide), 200)]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        public async Task<IActionResult> Get([FromRoute] int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var model = await db.TpVenteLiquide.FindAsync(key);

            if (model == null)
            {
                return NotFound();
            }
            return Ok(model);
        }

        // PUT: companies(1)
        /// <summary>
        /// Modifie un detail
        /// </summary>
        /// <param name="key"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(NoContentResult),204)]
        public async Task<IActionResult> Put([FromRoute] int key, [FromBody] TpVenteLiquide model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != model.IdVenteLiquide)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IfExists(key))
                {
                    return NotFound();
                }
                throw;
            }

            return NoContent();
        }
        /// <summary>
        /// Ajoute un detail
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // POST: companies
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NoContentResult),204)]
        public async Task<IActionResult> Post([FromBody] TpVenteLiquide model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(model).State = EntityState.Added;

            await db.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Supprime une vente
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        /// <response code="400">La requette n'est pas comprises par le serveur</response>
        /// <response code="404">Client introuvable pour l'id specifié</response>
        /// <response code="200">Requette executé avec succes</response>
        // DELETE: companies(1)
        [EnableQuery]
        [ProducesResponseType(typeof(BadRequestResult),400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
        [ProducesResponseType(typeof(TpVenteLiquide), 200)]
        public async Task<IActionResult> Delete([FromRoute] int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var model = await db.TpVenteLiquide.FindAsync(key);
            if (model == null)
            {
                return NotFound();
            }

            db.Entry(model).State = EntityState.Deleted;

            await db.SaveChangesAsync(); 
            return Ok(model);
        }
        /// <summary>
        /// Verifie si l'élément existe
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected bool IfExists(int key)
        {
            return db.TpVenteLiquide.Any(e => e.IdVenteLiquide == key);
        }

        [SwaggerIgnore]
        [HttpGet("VenteDetailDoc")]
        public async Task<ContentResult> Swagger()
        {
            var settings = new WebApiToSwaggerGeneratorSettings
            {
                DefaultUrlTemplate = "api/{controller}/{action}/{id}",
            };
            var generator = new WebApiToSwaggerGenerator(settings);
            var document = await generator.GenerateForControllerAsync<VenteDetailsController>();
            var swaggerSpecification = document.ToJson();
            return Content(document.ToJson());
        }
    }
}
