﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Ouverture_Cloture_Caisse")]
    public partial class TpOuvertureClotureCaisse
    {
        [Key]
        [Column("ID_Ouverture_Cloture_Caisse")]
        public int IdOuvertureClotureCaisse { get; set; }
        [Column("ID_Caisse")]
        public int IdCaisse { get; set; }
        [Column("Date_Ouverture", TypeName = "datetime")]
        public DateTime DateOuverture { get; set; }
        [Column("Date_Cloture", TypeName = "datetime")]
        public DateTime? DateCloture { get; set; }
        [Column("Solde_Caisse")]
        public double SoldeCaisse { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        public byte Etat { get; set; }
        [Column("ID_Personnel")]
        public int IdPersonnel { get; set; }

        [ForeignKey("IdCaisse")]
        [InverseProperty("TpOuvertureClotureCaisse")]
        public TpCaisse IdCaisseNavigation { get; set; }
        [ForeignKey("IdPersonnel")]
        [InverseProperty("TpOuvertureClotureCaisse")]
        public TgPersonnels IdPersonnelNavigation { get; set; }
    }
}
