﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PROJETMODEL.Models.DAL
{
    public partial class Store : DbContext
    {
        public Store()
        {
        }

        public Store(DbContextOptions<Store> options)
            : base(options)
        {
        }

        public virtual DbSet<TgCommandes> TgCommandes { get; set; }
        public virtual DbSet<TgCorbeille> TgCorbeille { get; set; }
        public virtual DbSet<TgDroits> TgDroits { get; set; }
        public virtual DbSet<TgHierarchieCommande> TgHierarchieCommande { get; set; }
        public virtual DbSet<TgLockedRecords> TgLockedRecords { get; set; }
        public virtual DbSet<TgLogs> TgLogs { get; set; }
        public virtual DbSet<TgPersonnels> TgPersonnels { get; set; }
        public virtual DbSet<TgProfiles> TgProfiles { get; set; }
        public virtual DbSet<TgProfilesUtilisateurs> TgProfilesUtilisateurs { get; set; }
        public virtual DbSet<TgSentinelles> TgSentinelles { get; set; }
        public virtual DbSet<TgSentinellesOptions> TgSentinellesOptions { get; set; }
        public virtual DbSet<TgUtilisateurs> TgUtilisateurs { get; set; }
        public virtual DbSet<TgVariable> TgVariable { get; set; }
        public virtual DbSet<TpCaisse> TpCaisse { get; set; }
        public virtual DbSet<TpCategorie> TpCategorie { get; set; }
        public virtual DbSet<TpClient> TpClient { get; set; }
        public virtual DbSet<TpCommande> TpCommande { get; set; }
        public virtual DbSet<TpCommandeEmballage> TpCommandeEmballage { get; set; }
        public virtual DbSet<TpCommandeEmballageTemp> TpCommandeEmballageTemp { get; set; }
        public virtual DbSet<TpCommandeLiquide> TpCommandeLiquide { get; set; }
        public virtual DbSet<TpCommandeLiquideTemp> TpCommandeLiquideTemp { get; set; }
        public virtual DbSet<TpDecaissement> TpDecaissement { get; set; }
        public virtual DbSet<TpEmballage> TpEmballage { get; set; }
        public virtual DbSet<TpEncaissement> TpEncaissement { get; set; }
        public virtual DbSet<TpFamille> TpFamille { get; set; }
        public virtual DbSet<TpFournisseur> TpFournisseur { get; set; }
        public virtual DbSet<TpInventaire> TpInventaire { get; set; }
        public virtual DbSet<TpInventaireDetail> TpInventaireDetail { get; set; }
        public virtual DbSet<TpOuvertureClotureCaisse> TpOuvertureClotureCaisse { get; set; }
        public virtual DbSet<TpProduit> TpProduit { get; set; }
        public virtual DbSet<TpRegulationSolde> TpRegulationSolde { get; set; }
        public virtual DbSet<TpRegulationStock> TpRegulationStock { get; set; }
        public virtual DbSet<TpRetourEmballage> TpRetourEmballage { get; set; }
        public virtual DbSet<TpRetourEmballageDetail> TpRetourEmballageDetail { get; set; }
        public virtual DbSet<TpRetourEmballageTemp> TpRetourEmballageTemp { get; set; }
        public virtual DbSet<TpRistourneClient> TpRistourneClient { get; set; }
        public virtual DbSet<TpTypeDepense> TpTypeDepense { get; set; }
        public virtual DbSet<TpTypeProduit> TpTypeProduit { get; set; }
        public virtual DbSet<TpVente> TpVente { get; set; }
        public virtual DbSet<TpVenteEmballage> TpVenteEmballage { get; set; }
        public virtual DbSet<TpVenteEmballageTemp> TpVenteEmballageTemp { get; set; }
        public virtual DbSet<TpVenteLiquide> TpVenteLiquide { get; set; }
        public virtual DbSet<TpVenteLiquideTemp> TpVenteLiquideTemp { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=(local);Initial Catalog=BD_SNACK;User ID=admin;Password=admin;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TgCorbeille>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.Property(e => e.Date).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TgDroits>(entity =>
            {
                entity.Property(e => e.Disponible).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CodeCommandeNavigation)
                    .WithMany(p => p.TgDroits)
                    .HasForeignKey(d => d.CodeCommande)
                    .HasConstraintName("TG_Commandes_TG_Droits_FK1");

                entity.HasOne(d => d.CodeProfileNavigation)
                    .WithMany(p => p.TgDroits)
                    .HasForeignKey(d => d.CodeProfile)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Profiles_TG_Droits_FK1");
            });

            modelBuilder.Entity<TgHierarchieCommande>(entity =>
            {
                entity.HasOne(d => d.CodeCommandeParentNavigation)
                    .WithMany(p => p.TgHierarchieCommande)
                    .HasForeignKey(d => d.CodeCommandeParent)
                    .HasConstraintName("FK_TG_Hierarchie_Commande_TG_Commandes1");
            });

            modelBuilder.Entity<TgLockedRecords>(entity =>
            {
                entity.Property(e => e.LockDateTime).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<TgPersonnels>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TgPersonnels)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TG_Personnels_TP_Caisse");
            });

            modelBuilder.Entity<TgProfiles>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TgProfilesUtilisateurs>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.CodeProfileNavigation)
                    .WithMany(p => p.TgProfilesUtilisateurs)
                    .HasForeignKey(d => d.CodeProfile)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Profiles_TG_Profiles_Utilisateurs_FK1");

                entity.HasOne(d => d.CodeUserNavigation)
                    .WithMany(p => p.TgProfilesUtilisateurs)
                    .HasForeignKey(d => d.CodeUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Utilisateurs_TG_Profiles_Utilisateurs_FK1");
            });

            modelBuilder.Entity<TgUtilisateurs>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdPersonnelNavigation)
                    .WithMany(p => p.TgUtilisateurs)
                    .HasForeignKey(d => d.IdPersonnel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TG_Personnels_TG_Utilisateurs_FK1");
            });

            modelBuilder.Entity<TpCaisse>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpCategorie>(entity =>
            {
                entity.Property(e => e.IdCategorie).ValueGeneratedOnAdd();

                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpClient>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpCommande>(entity =>
            {
                entity.HasOne(d => d.IdFournisseurNavigation)
                    .WithMany(p => p.TpCommande)
                    .HasForeignKey(d => d.IdFournisseur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_TP_Fournisseur");
            });

            modelBuilder.Entity<TpCommandeEmballage>(entity =>
            {
                entity.HasOne(d => d.IdCommandeNavigation)
                    .WithMany(p => p.TpCommandeEmballage)
                    .HasForeignKey(d => d.IdCommande)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Emballage_TP_Commande");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpCommandeEmballage)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Emballage_TP_Emballage");
            });

            modelBuilder.Entity<TpCommandeEmballageTemp>(entity =>
            {
                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpCommandeEmballageTemp)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Emballage_Temp_TP_Emballage");
            });

            modelBuilder.Entity<TpCommandeLiquide>(entity =>
            {
                entity.HasOne(d => d.IdCommandeNavigation)
                    .WithMany(p => p.TpCommandeLiquide)
                    .HasForeignKey(d => d.IdCommande)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Liquide_TP_Commande");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpCommandeLiquide)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Liquide_TP_Produit");
            });

            modelBuilder.Entity<TpCommandeLiquideTemp>(entity =>
            {
                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpCommandeLiquideTemp)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Commande_Liquide_Temp_TP_Produit");
            });

            modelBuilder.Entity<TpDecaissement>(entity =>
            {
                entity.Property(e => e.Etat).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TpDecaissement)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Decaissement_TP_Caisse");
            });

            modelBuilder.Entity<TpEmballage>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpEncaissement>(entity =>
            {
                entity.Property(e => e.Etat).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdCaissseNavigation)
                    .WithMany(p => p.TpEncaissement)
                    .HasForeignKey(d => d.IdCaissse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Encaissement_TP_Caisse");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpEncaissement)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Encaissement_TP_Client");
            });

            modelBuilder.Entity<TpFamille>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpFournisseur>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpInventaireDetail>(entity =>
            {
                entity.HasOne(d => d.IdInventaireNavigation)
                    .WithMany(p => p.TpInventaireDetail)
                    .HasForeignKey(d => d.IdInventaire)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Inventaire_Detail_TP_Inventaire");
            });

            modelBuilder.Entity<TpOuvertureClotureCaisse>(entity =>
            {
                entity.Property(e => e.Etat).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TpOuvertureClotureCaisse)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ouverture_Cloture_Caisse_TP_Caisse");

                entity.HasOne(d => d.IdPersonnelNavigation)
                    .WithMany(p => p.TpOuvertureClotureCaisse)
                    .HasForeignKey(d => d.IdPersonnel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ouverture_Cloture_Caisse_TG_Personnels");
            });

            modelBuilder.Entity<TpProduit>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdBouteilleBaseNavigation)
                    .WithMany(p => p.TpProduitIdBouteilleBaseNavigation)
                    .HasForeignKey(d => d.IdBouteilleBase)
                    .HasConstraintName("FK_TP_Produit_TP_Emballage2");

                entity.HasOne(d => d.IdCategorieNavigation)
                    .WithMany(p => p.TpProduit)
                    .HasForeignKey(d => d.IdCategorie)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Produit_TP_Categorie");

                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpProduitIdEmballageNavigation)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Produit_TP_Emballage");

                entity.HasOne(d => d.IdFamilleNavigation)
                    .WithMany(p => p.TpProduit)
                    .HasForeignKey(d => d.IdFamille)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Produit_TP_Producteur");

                entity.HasOne(d => d.IdPlastiqueBaseNavigation)
                    .WithMany(p => p.TpProduitIdPlastiqueBaseNavigation)
                    .HasForeignKey(d => d.IdPlastiqueBase)
                    .HasConstraintName("FK_TP_Produit_TP_Emballage1");

                entity.HasOne(d => d.IdProduitBaseNavigation)
                    .WithMany(p => p.InverseIdProduitBaseNavigation)
                    .HasForeignKey(d => d.IdProduitBase)
                    .HasConstraintName("FK_TP_Produit_TP_Produit");

                entity.HasOne(d => d.IdTypeProduitNavigation)
                    .WithMany(p => p.TpProduit)
                    .HasForeignKey(d => d.IdTypeProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Produit_TP_Type_Produit");
            });

            modelBuilder.Entity<TpRetourEmballageDetail>(entity =>
            {
                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpRetourEmballageDetail)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Retour_Emballage_Detail_TP_Emballage");

                entity.HasOne(d => d.IdRetourEmballageNavigation)
                    .WithMany(p => p.TpRetourEmballageDetail)
                    .HasForeignKey(d => d.IdRetourEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Retour_Emballage_Detail_TP_Retour_Emballage");
            });

            modelBuilder.Entity<TpRetourEmballageTemp>(entity =>
            {
                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpRetourEmballageTemp)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Retour_Emballage_Fournisseur_Temp_TP_Emballage");
            });

            modelBuilder.Entity<TpRistourneClient>(entity =>
            {
                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpRistourneClient)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ristourne_Client_TP_Client");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpRistourneClient)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Ristourne_Client_TP_Produit");
            });

            modelBuilder.Entity<TpTypeDepense>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpTypeProduit>(entity =>
            {
                entity.Property(e => e.Actif).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<TpVente>(entity =>
            {
                entity.HasOne(d => d.IdCaisseNavigation)
                    .WithMany(p => p.TpVente)
                    .HasForeignKey(d => d.IdCaisse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_TP_Caisse");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpVente)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_TP_Client");
            });

            modelBuilder.Entity<TpVenteEmballage>(entity =>
            {
                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpVenteEmballage)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Emballage_TP_Emballage");

                entity.HasOne(d => d.IdVenteNavigation)
                    .WithMany(p => p.TpVenteEmballage)
                    .HasForeignKey(d => d.IdVente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Emballage_TP_Vente");
            });

            modelBuilder.Entity<TpVenteEmballageTemp>(entity =>
            {
                entity.HasOne(d => d.IdEmballageNavigation)
                    .WithMany(p => p.TpVenteEmballageTemp)
                    .HasForeignKey(d => d.IdEmballage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Emballage_Temp_TP_Emballage");
            });

            modelBuilder.Entity<TpVenteLiquide>(entity =>
            {
                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpVenteLiquide)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_TP_Produit");

                entity.HasOne(d => d.IdVenteNavigation)
                    .WithMany(p => p.TpVenteLiquide)
                    .HasForeignKey(d => d.IdVente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_TP_Vente");
            });

            modelBuilder.Entity<TpVenteLiquideTemp>(entity =>
            {
                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.TpVenteLiquideTemp)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_Temp_TP_Client");

                entity.HasOne(d => d.IdProduitNavigation)
                    .WithMany(p => p.TpVenteLiquideTemp)
                    .HasForeignKey(d => d.IdProduit)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TP_Vente_Liquide_Temp_TP_Produit");
            });
        }
    }
}
