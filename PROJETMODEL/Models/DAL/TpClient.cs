﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Client")]
    public partial class TpClient
    {
        public TpClient()
        {
            TpEncaissement = new HashSet<TpEncaissement>();
            TpRistourneClient = new HashSet<TpRistourneClient>();
            TpVente = new HashSet<TpVente>();
            TpVenteLiquideTemp = new HashSet<TpVenteLiquideTemp>();
        }

        [Key]
        [Column("ID_Client")]
        public int IdClient { get; set; }
        [Required]
        [StringLength(150)]
        public string Nom { get; set; }
        [StringLength(50)]
        public string Adresse { get; set; }
        [StringLength(50)]
        public string Telephone { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [InverseProperty("IdClientNavigation")]
        public ICollection<TpEncaissement> TpEncaissement { get; set; }
        [InverseProperty("IdClientNavigation")]
        public ICollection<TpRistourneClient> TpRistourneClient { get; set; }
        [InverseProperty("IdClientNavigation")]
        public ICollection<TpVente> TpVente { get; set; }
        [InverseProperty("IdClientNavigation")]
        public ICollection<TpVenteLiquideTemp> TpVenteLiquideTemp { get; set; }
    }
}
