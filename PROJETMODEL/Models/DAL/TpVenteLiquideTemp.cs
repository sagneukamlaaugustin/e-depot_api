﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Vente_Liquide_Temp")]
    public partial class TpVenteLiquideTemp
    {
        [Key]
        [Column("ID_Vente_Liquide_Temp")]
        public long IdVenteLiquideTemp { get; set; }
        [Column("ID_Client")]
        public int IdClient { get; set; }
        [Column("ID_Produit")]
        public int IdProduit { get; set; }
        [Column("PU_Vente")]
        public double? PuVente { get; set; }
        [Column("PU_Achat")]
        public double? PuAchat { get; set; }
        public int? Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double? PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double? PuBouteille { get; set; }
        [Column("Quantite_Casier_Liquide")]
        public int QuantiteCasierLiquide { get; set; }
        [Column("Quantite_Bouteille_Liquide")]
        public int QuantiteBouteilleLiquide { get; set; }
        [Column("Quantite_Casier_Emballage")]
        public int QuantiteCasierEmballage { get; set; }
        [Column("Quantite_Plastique_Emballage")]
        public int QuantitePlastiqueEmballage { get; set; }
        [Column("Quantite_Bouteille_Emballage")]
        public int QuantiteBouteilleEmballage { get; set; }
        [Column("Ristourne_Client")]
        public double? RistourneClient { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Produit_Decomposer")]
        public bool? ProduitDecomposer { get; set; }
        [Column("Cumuler_Ristourne")]
        public bool? CumulerRistourne { get; set; }

        [ForeignKey("IdClient")]
        [InverseProperty("TpVenteLiquideTemp")]
        public TpClient IdClientNavigation { get; set; }
        [ForeignKey("IdProduit")]
        [InverseProperty("TpVenteLiquideTemp")]
        public TpProduit IdProduitNavigation { get; set; }
    }
}
