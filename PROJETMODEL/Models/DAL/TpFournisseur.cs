﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Fournisseur")]
    public partial class TpFournisseur
    {
        public TpFournisseur()
        {
            TpCommande = new HashSet<TpCommande>();
        }

        [Key]
        [Column("ID_Fournisseur")]
        public int IdFournisseur { get; set; }
        [Required]
        [StringLength(150)]
        public string Nom { get; set; }
        [StringLength(50)]
        public string Adresse { get; set; }
        [StringLength(50)]
        public string Telephone { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [InverseProperty("IdFournisseurNavigation")]
        public ICollection<TpCommande> TpCommande { get; set; }
    }
}
