﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Droits")]
    public partial class TgDroits
    {
        [Key]
        [Column("ID_Droit")]
        public int IdDroit { get; set; }
        [Column("Code_Profile")]
        public int CodeProfile { get; set; }
        [Column("Code_Commande")]
        public int CodeCommande { get; set; }
        public bool Executer { get; set; }
        [Required]
        public bool? Disponible { get; set; }

        [ForeignKey("CodeCommande")]
        [InverseProperty("TgDroits")]
        public TgCommandes CodeCommandeNavigation { get; set; }
        [ForeignKey("CodeProfile")]
        [InverseProperty("TgDroits")]
        public TgProfiles CodeProfileNavigation { get; set; }
    }
}
