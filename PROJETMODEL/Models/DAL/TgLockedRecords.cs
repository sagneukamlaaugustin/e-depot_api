﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Locked_Records")]
    public partial class TgLockedRecords
    {
        [Key]
        [Column("ID_Locked")]
        public int IdLocked { get; set; }
        [Required]
        [Column("Table_Name")]
        [StringLength(50)]
        public string TableName { get; set; }
        [Column("Record_ID")]
        public int RecordId { get; set; }
        [Required]
        [Column("Computer_Name")]
        [StringLength(50)]
        public string ComputerName { get; set; }
        [Column("Lock_Date_Time", TypeName = "datetime")]
        public DateTime LockDateTime { get; set; }
        [Column("Code_User")]
        public int CodeUser { get; set; }
    }
}
