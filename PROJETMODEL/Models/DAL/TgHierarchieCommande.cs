﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Hierarchie_Commande")]
    public partial class TgHierarchieCommande
    {
        [Key]
        [Column("ID_Hierarchie")]
        public int IdHierarchie { get; set; }
        [Column("Code_Commande")]
        public int CodeCommande { get; set; }
        [Column("Code_Commande_Parent")]
        public int CodeCommandeParent { get; set; }

        [ForeignKey("CodeCommandeParent")]
        [InverseProperty("TgHierarchieCommande")]
        public TgCommandes CodeCommandeParentNavigation { get; set; }
    }
}
