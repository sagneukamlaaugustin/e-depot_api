﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Regulation_Stock")]
    public partial class TpRegulationStock
    {
        [Key]
        [Column("ID_Regulation_Stock")]
        public long IdRegulationStock { get; set; }
        [Column("ID_Element")]
        public int IdElement { get; set; }
        [Column("Type_Element")]
        public byte TypeElement { get; set; }
        [Column("NUM_DOC")]
        [StringLength(50)]
        public string NumDoc { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("Quantite_Casier")]
        public int QuantiteCasier { get; set; }
        [Column("Quantite_Plastique")]
        public int QuantitePlastique { get; set; }
        [Column("Quantite_Bouteille")]
        public int QuantiteBouteille { get; set; }
        [StringLength(500)]
        public string Motif { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        public byte Etat { get; set; }
    }
}
