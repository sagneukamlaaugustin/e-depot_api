﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Variable")]
    public partial class TgVariable
    {
        [Key]
        [Column("ID_Variable")]
        public int IdVariable { get; set; }
        [Required]
        [Column("Nom_Societe")]
        [StringLength(150)]
        public string NomSociete { get; set; }
        [Column("Numero_Contribuable")]
        [StringLength(50)]
        public string NumeroContribuable { get; set; }
        [Column("Registre_Commerce")]
        [StringLength(50)]
        public string RegistreCommerce { get; set; }
        [StringLength(50)]
        public string Telephone { get; set; }
        [StringLength(50)]
        public string Adresse { get; set; }
        [StringLength(150)]
        public string Description { get; set; }
        [StringLength(50)]
        public string Ville { get; set; }
        public byte[] Logo { get; set; }
        [Column("Appliquer_TVA")]
        public bool? AppliquerTva { get; set; }
        [Column("TVA")]
        public double? Tva { get; set; }
        [Column("Gestion_Emballage")]
        public bool? GestionEmballage { get; set; }
        [Column("Verifier_Etat_Stock")]
        public bool? VerifierEtatStock { get; set; }
        [StringLength(100)]
        public string Cle0 { get; set; }
        [MaxLength(100)]
        public byte[] Cle1 { get; set; }
        [MaxLength(100)]
        public byte[] Cle2 { get; set; }
        [Column("Dossier_Sauvegarde")]
        [StringLength(150)]
        public string DossierSauvegarde { get; set; }
        [Column("Password_Sauvegarde")]
        [StringLength(50)]
        public string PasswordSauvegarde { get; set; }
    }
}
