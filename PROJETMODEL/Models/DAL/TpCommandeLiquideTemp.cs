﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Commande_Liquide_Temp")]
    public partial class TpCommandeLiquideTemp
    {
        [Key]
        [Column("ID_Commande_Liquide_Temp")]
        public long IdCommandeLiquideTemp { get; set; }
        [Column("ID_Produit")]
        public int IdProduit { get; set; }
        [Column("PU_Achat")]
        public double? PuAchat { get; set; }
        public int? Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double? PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double? PuBouteille { get; set; }
        [Column("Quantite_Casier_Liquide")]
        public int QuantiteCasierLiquide { get; set; }
        [Column("Quantite_Bouteille_Liquide")]
        public int QuantiteBouteilleLiquide { get; set; }
        [Column("Quantite_Casier_Emballage")]
        public int QuantiteCasierEmballage { get; set; }
        [Column("Quantite_Plastique_Emballage")]
        public int QuantitePlastiqueEmballage { get; set; }
        [Column("Quantite_Bouteille_Emballage")]
        public int QuantiteBouteilleEmballage { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }

        [ForeignKey("IdProduit")]
        [InverseProperty("TpCommandeLiquideTemp")]
        public TpProduit IdProduitNavigation { get; set; }
    }
}
