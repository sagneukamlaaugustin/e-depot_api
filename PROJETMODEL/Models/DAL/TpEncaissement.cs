﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Encaissement")]
    public partial class TpEncaissement
    {
        [Key]
        [Column("ID_Encaissement")]
        public long IdEncaissement { get; set; }
        [Column("ID_Client")]
        public int IdClient { get; set; }
        [Column("ID_Caissse")]
        public int IdCaissse { get; set; }
        [Column("NUM_DOC")]
        [StringLength(50)]
        public string NumDoc { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public double Montant { get; set; }
        [StringLength(500)]
        public string Motif { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        public byte Etat { get; set; }

        [ForeignKey("IdCaissse")]
        [InverseProperty("TpEncaissement")]
        public TpCaisse IdCaissseNavigation { get; set; }
        [ForeignKey("IdClient")]
        [InverseProperty("TpEncaissement")]
        public TpClient IdClientNavigation { get; set; }
    }
}
