﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Famille")]
    public partial class TpFamille
    {
        public TpFamille()
        {
            TpProduit = new HashSet<TpProduit>();
        }

        [Key]
        [Column("ID_Famille")]
        public int IdFamille { get; set; }
        [Required]
        [StringLength(50)]
        public string Libelle { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [InverseProperty("IdFamilleNavigation")]
        public ICollection<TpProduit> TpProduit { get; set; }
    }
}
