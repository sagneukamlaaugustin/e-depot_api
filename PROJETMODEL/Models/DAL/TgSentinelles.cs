﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Sentinelles")]
    public partial class TgSentinelles
    {
        [Key]
        [Column("ID_Sentinelle")]
        public int IdSentinelle { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("Code_User")]
        public int CodeUser { get; set; }
        public int Action { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }
    }
}
