﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Sentinelles_Options")]
    public partial class TgSentinellesOptions
    {
        [Key]
        [Column("ID_Sentinelle")]
        public int IdSentinelle { get; set; }
        public bool Sentinelle { get; set; }
        public bool SentinelleConnexion { get; set; }
        public bool SentinelleEndConnexion { get; set; }
        public bool SentinelleInsert { get; set; }
        public bool SentinelleEdit { get; set; }
        public bool SentinelleDelete { get; set; }
        public bool SentinellePrint { get; set; }
        public int SentinelleLife { get; set; }
    }
}
