﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Profiles_Utilisateurs")]
    public partial class TgProfilesUtilisateurs
    {
        [Key]
        [Column("ID_Profiles_Utilisateur")]
        public int IdProfilesUtilisateur { get; set; }
        [Column("Code_User")]
        public int CodeUser { get; set; }
        [Column("Code_Profile")]
        public int CodeProfile { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [ForeignKey("CodeProfile")]
        [InverseProperty("TgProfilesUtilisateurs")]
        public TgProfiles CodeProfileNavigation { get; set; }
        [ForeignKey("CodeUser")]
        [InverseProperty("TgProfilesUtilisateurs")]
        public TgUtilisateurs CodeUserNavigation { get; set; }
    }
}
