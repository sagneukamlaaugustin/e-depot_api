﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Logs")]
    public partial class TgLogs
    {
        [Key]
        [Column("Id_Error")]
        public int IdError { get; set; }
        [Required]
        [Column("Error_value")]
        public string ErrorValue { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("Code_user")]
        public int CodeUser { get; set; }
        [Required]
        [Column("Nom_Poste_Travail")]
        [StringLength(50)]
        public string NomPosteTravail { get; set; }
        [Required]
        [StringLength(50)]
        public string Fonction { get; set; }
    }
}
