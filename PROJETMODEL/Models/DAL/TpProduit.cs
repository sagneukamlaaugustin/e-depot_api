﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Produit")]
    public partial class TpProduit
    {
        public TpProduit()
        {
            InverseIdProduitBaseNavigation = new HashSet<TpProduit>();
            TpCommandeLiquide = new HashSet<TpCommandeLiquide>();
            TpCommandeLiquideTemp = new HashSet<TpCommandeLiquideTemp>();
            TpRistourneClient = new HashSet<TpRistourneClient>();
            TpVenteLiquide = new HashSet<TpVenteLiquide>();
            TpVenteLiquideTemp = new HashSet<TpVenteLiquideTemp>();
        }

        [Key]
        [Column("ID_Produit")]
        public int IdProduit { get; set; }
        [Column("ID_Produit_Base")]
        public int? IdProduitBase { get; set; }
        [Column("ID_Famille")]
        public int IdFamille { get; set; }
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Column("ID_Type_Produit")]
        public int IdTypeProduit { get; set; }
        [Column("ID_Categorie")]
        public byte IdCategorie { get; set; }
        [Required]
        [StringLength(50)]
        public string Code { get; set; }
        [Required]
        [StringLength(150)]
        public string Libelle { get; set; }
        [Column("PU_Achat")]
        public double PuAchat { get; set; }
        [Column("PU_Vente")]
        public double PuVente { get; set; }
        [Column("Valeur_Emballage")]
        public bool ValeurEmballage { get; set; }
        [Column("Produit_Decomposer")]
        public bool ProduitDecomposer { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }
        [Column("ID_Plastique_Base")]
        public int? IdPlastiqueBase { get; set; }
        [Column("ID_Bouteille_Base")]
        public int? IdBouteilleBase { get; set; }
        public byte[] Image { get; set; }

        [ForeignKey("IdBouteilleBase")]
        [InverseProperty("TpProduitIdBouteilleBaseNavigation")]
        public TpEmballage IdBouteilleBaseNavigation { get; set; }
        [ForeignKey("IdCategorie")]
        [InverseProperty("TpProduit")]
        public TpCategorie IdCategorieNavigation { get; set; }
        [ForeignKey("IdEmballage")]
        [InverseProperty("TpProduitIdEmballageNavigation")]
        public TpEmballage IdEmballageNavigation { get; set; }
        [ForeignKey("IdFamille")]
        [InverseProperty("TpProduit")]
        public TpFamille IdFamilleNavigation { get; set; }
        [ForeignKey("IdPlastiqueBase")]
        [InverseProperty("TpProduitIdPlastiqueBaseNavigation")]
        public TpEmballage IdPlastiqueBaseNavigation { get; set; }
        [ForeignKey("IdProduitBase")]
        [InverseProperty("InverseIdProduitBaseNavigation")]
        public TpProduit IdProduitBaseNavigation { get; set; }
        [ForeignKey("IdTypeProduit")]
        [InverseProperty("TpProduit")]
        public TpTypeProduit IdTypeProduitNavigation { get; set; }
        [InverseProperty("IdProduitBaseNavigation")]
        public ICollection<TpProduit> InverseIdProduitBaseNavigation { get; set; }
        [InverseProperty("IdProduitNavigation")]
        public ICollection<TpCommandeLiquide> TpCommandeLiquide { get; set; }
        [InverseProperty("IdProduitNavigation")]
        public ICollection<TpCommandeLiquideTemp> TpCommandeLiquideTemp { get; set; }
        [InverseProperty("IdProduitNavigation")]
        public ICollection<TpRistourneClient> TpRistourneClient { get; set; }
        [InverseProperty("IdProduitNavigation")]
        public ICollection<TpVenteLiquide> TpVenteLiquide { get; set; }
        [InverseProperty("IdProduitNavigation")]
        public ICollection<TpVenteLiquideTemp> TpVenteLiquideTemp { get; set; }
    }
}
