﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Vente")]
    public partial class TpVente
    {
        public TpVente()
        {
            TpVenteEmballage = new HashSet<TpVenteEmballage>();
            TpVenteLiquide = new HashSet<TpVenteLiquide>();
        }

        [Key]
        [Column("ID_Vente")]
        public long IdVente { get; set; }
        [Column("ID_Client")]
        public int IdClient { get; set; }
        [Column("ID_Caisse")]
        public int IdCaisse { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("NUM_DOC")]
        [StringLength(50)]
        public string NumDoc { get; set; }
        [Required]
        [Column("Nom_Client")]
        [StringLength(150)]
        public string NomClient { get; set; }
        [Column("Montant_Consigne_Emballage")]
        public double MontantConsigneEmballage { get; set; }
        public double Montant { get; set; }
        [Column("Solde_Liquide")]
        public double? SoldeLiquide { get; set; }
        [Column("Solde_Emballage")]
        public double? SoldeEmballage { get; set; }
        public byte Etat { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }

        [ForeignKey("IdCaisse")]
        [InverseProperty("TpVente")]
        public TpCaisse IdCaisseNavigation { get; set; }
        [ForeignKey("IdClient")]
        [InverseProperty("TpVente")]
        public TpClient IdClientNavigation { get; set; }
        [InverseProperty("IdVenteNavigation")]
        public ICollection<TpVenteEmballage> TpVenteEmballage { get; set; }
        [InverseProperty("IdVenteNavigation")]
        public ICollection<TpVenteLiquide> TpVenteLiquide { get; set; }
    }
}
