﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Vente_Emballage")]
    public partial class TpVenteEmballage
    {
        [Key]
        [Column("ID_Vente_Emballage")]
        public long IdVenteEmballage { get; set; }
        [Column("ID_Vente")]
        public long IdVente { get; set; }
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Column("Quantite_Casier")]
        public int QuantiteCasier { get; set; }
        [Column("Quantite_Plastique")]
        public int QuantitePlastique { get; set; }
        [Column("Quantite_Bouteille")]
        public int QuantiteBouteille { get; set; }
        public int Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double PuBouteille { get; set; }

        [ForeignKey("IdEmballage")]
        [InverseProperty("TpVenteEmballage")]
        public TpEmballage IdEmballageNavigation { get; set; }
        [ForeignKey("IdVente")]
        [InverseProperty("TpVenteEmballage")]
        public TpVente IdVenteNavigation { get; set; }
    }
}
