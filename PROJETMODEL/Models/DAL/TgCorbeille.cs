﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Corbeille")]
    public partial class TgCorbeille
    {
        [Key]
        [Column("ID_Corbeille")]
        public int IdCorbeille { get; set; }
        [Column("ID_Table")]
        public int IdTable { get; set; }
        [Column("ID_Element")]
        public int IdElement { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        [Column("Code_User")]
        public int CodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }
    }
}
