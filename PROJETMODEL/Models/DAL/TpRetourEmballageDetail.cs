﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Retour_Emballage_Detail")]
    public partial class TpRetourEmballageDetail
    {
        [Key]
        [Column("ID_Retour_Emballage_Detail")]
        public long IdRetourEmballageDetail { get; set; }
        [Column("ID_Retour_Emballage")]
        public long IdRetourEmballage { get; set; }
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Column("PU_Plastique")]
        public double PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double PuBouteille { get; set; }
        public int Conditionnement { get; set; }
        [Column("Quantite_Casier")]
        public int QuantiteCasier { get; set; }
        [Column("Quantite_Plastique")]
        public int QuantitePlastique { get; set; }
        [Column("Quantite_Bouteille")]
        public int QuantiteBouteille { get; set; }

        [ForeignKey("IdEmballage")]
        [InverseProperty("TpRetourEmballageDetail")]
        public TpEmballage IdEmballageNavigation { get; set; }
        [ForeignKey("IdRetourEmballage")]
        [InverseProperty("TpRetourEmballageDetail")]
        public TpRetourEmballage IdRetourEmballageNavigation { get; set; }
    }
}
