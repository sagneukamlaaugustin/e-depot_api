﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Personnels")]
    public partial class TgPersonnels
    {
        public TgPersonnels()
        {
            TgUtilisateurs = new HashSet<TgUtilisateurs>();
            TpOuvertureClotureCaisse = new HashSet<TpOuvertureClotureCaisse>();
        }

        [Key]
        [Column("ID_Personnel")]
        public int IdPersonnel { get; set; }
        [Column("ID_Caisse")]
        public int IdCaisse { get; set; }
        [Required]
        [StringLength(150)]
        public string Nom { get; set; }
        [StringLength(150)]
        public string Prenom { get; set; }
        [Required]
        [StringLength(20)]
        public string Telephone { get; set; }
        [StringLength(150)]
        public string Email { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }
        public bool Display { get; set; }

        [ForeignKey("IdCaisse")]
        [InverseProperty("TgPersonnels")]
        public TpCaisse IdCaisseNavigation { get; set; }
        [InverseProperty("IdPersonnelNavigation")]
        public ICollection<TgUtilisateurs> TgUtilisateurs { get; set; }
        [InverseProperty("IdPersonnelNavigation")]
        public ICollection<TpOuvertureClotureCaisse> TpOuvertureClotureCaisse { get; set; }
    }
}
