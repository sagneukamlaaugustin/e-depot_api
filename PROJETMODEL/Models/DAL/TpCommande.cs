﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Commande")]
    public partial class TpCommande
    {
        public TpCommande()
        {
            TpCommandeEmballage = new HashSet<TpCommandeEmballage>();
            TpCommandeLiquide = new HashSet<TpCommandeLiquide>();
        }

        [Key]
        [Column("ID_Commande")]
        public long IdCommande { get; set; }
        [Column("ID_Fournisseur")]
        public int IdFournisseur { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public double? Montant { get; set; }
        [Column("NUM_DOC")]
        [StringLength(50)]
        public string NumDoc { get; set; }
        public byte Etat { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }

        [ForeignKey("IdFournisseur")]
        [InverseProperty("TpCommande")]
        public TpFournisseur IdFournisseurNavigation { get; set; }
        [InverseProperty("IdCommandeNavigation")]
        public ICollection<TpCommandeEmballage> TpCommandeEmballage { get; set; }
        [InverseProperty("IdCommandeNavigation")]
        public ICollection<TpCommandeLiquide> TpCommandeLiquide { get; set; }
    }
}
