﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Commandes")]
    public partial class TgCommandes
    {
        public TgCommandes()
        {
            TgDroits = new HashSet<TgDroits>();
            TgHierarchieCommande = new HashSet<TgHierarchieCommande>();
        }

        [Key]
        [Column("Code_Commande")]
        public int CodeCommande { get; set; }
        [Column("Code_Commande_Parent")]
        public int CodeCommandeParent { get; set; }
        [Required]
        [StringLength(50)]
        public string Libelle { get; set; }
        [Required]
        [Column("ID_Control")]
        [StringLength(50)]
        public string IdControl { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public int Ordre { get; set; }
        [Column("url")]
        [StringLength(50)]
        public string Url { get; set; }

        [InverseProperty("CodeCommandeNavigation")]
        public ICollection<TgDroits> TgDroits { get; set; }
        [InverseProperty("CodeCommandeParentNavigation")]
        public ICollection<TgHierarchieCommande> TgHierarchieCommande { get; set; }
    }
}
