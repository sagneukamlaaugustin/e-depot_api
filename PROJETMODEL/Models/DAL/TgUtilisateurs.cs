﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Utilisateurs")]
    public partial class TgUtilisateurs
    {
        public TgUtilisateurs()
        {
            TgProfilesUtilisateurs = new HashSet<TgProfilesUtilisateurs>();
        }

        [Key]
        [Column("Code_User")]
        public int CodeUser { get; set; }
        [Column("ID_Personnel")]
        public int IdPersonnel { get; set; }
        [Required]
        [StringLength(50)]
        public string Login { get; set; }
        [Required]
        [Column("Somme_Ctrl")]
        [StringLength(50)]
        public string SommeCtrl { get; set; }
        public bool Display { get; set; }
        [Column("Last_Connexion", TypeName = "datetime")]
        public DateTime LastConnexion { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [ForeignKey("IdPersonnel")]
        [InverseProperty("TgUtilisateurs")]
        public TgPersonnels IdPersonnelNavigation { get; set; }
        [InverseProperty("CodeUserNavigation")]
        public ICollection<TgProfilesUtilisateurs> TgProfilesUtilisateurs { get; set; }
    }
}
