﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Inventaire")]
    public partial class TpInventaire
    {
        public TpInventaire()
        {
            TpInventaireDetail = new HashSet<TpInventaireDetail>();
        }

        [Key]
        [Column("ID_Inventaire")]
        public long IdInventaire { get; set; }
        public byte Type { get; set; }
        [Column("NUM_DOC")]
        [StringLength(50)]
        public string NumDoc { get; set; }
        [Column("Date_Debut", TypeName = "datetime")]
        public DateTime DateDebut { get; set; }
        [Column("Date_Fin", TypeName = "datetime")]
        public DateTime? DateFin { get; set; }
        public byte Etat { get; set; }
        [StringLength(500)]
        public string Motif { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }

        [InverseProperty("IdInventaireNavigation")]
        public ICollection<TpInventaireDetail> TpInventaireDetail { get; set; }
    }
}
