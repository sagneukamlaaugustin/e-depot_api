﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TG_Profiles")]
    public partial class TgProfiles
    {
        public TgProfiles()
        {
            TgDroits = new HashSet<TgDroits>();
            TgProfilesUtilisateurs = new HashSet<TgProfilesUtilisateurs>();
        }

        [Key]
        [Column("Code_Profile")]
        public int CodeProfile { get; set; }
        [Required]
        [StringLength(50)]
        public string Libelle { get; set; }
        public bool Display { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [InverseProperty("CodeProfileNavigation")]
        public ICollection<TgDroits> TgDroits { get; set; }
        [InverseProperty("CodeProfileNavigation")]
        public ICollection<TgProfilesUtilisateurs> TgProfilesUtilisateurs { get; set; }
    }
}
