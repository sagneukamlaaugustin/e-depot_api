﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Commande_Liquide")]
    public partial class TpCommandeLiquide
    {
        [Key]
        [Column("ID_Commande_Liquide")]
        public long IdCommandeLiquide { get; set; }
        [Column("ID_Commande")]
        public long IdCommande { get; set; }
        [Column("ID_Produit")]
        public int IdProduit { get; set; }
        [Column("PU_Achat")]
        public double? PuAchat { get; set; }
        public int? Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double? PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double? PuBouteille { get; set; }
        [Column("Quantite_Casier_Liquide")]
        public int QuantiteCasierLiquide { get; set; }
        [Column("Quantite_Bouteille_Liquide")]
        public int QuantiteBouteilleLiquide { get; set; }
        [Column("Quantite_Casier_Emballage")]
        public int QuantiteCasierEmballage { get; set; }
        [Column("Quantite_Plastique_Emballage")]
        public int QuantitePlastiqueEmballage { get; set; }
        [Column("Quantite_Bouteille_Emballage")]
        public int QuantiteBouteilleEmballage { get; set; }

        [ForeignKey("IdCommande")]
        [InverseProperty("TpCommandeLiquide")]
        public TpCommande IdCommandeNavigation { get; set; }
        [ForeignKey("IdProduit")]
        [InverseProperty("TpCommandeLiquide")]
        public TpProduit IdProduitNavigation { get; set; }
    }
}
