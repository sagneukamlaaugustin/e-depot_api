﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Inventaire_Detail")]
    public partial class TpInventaireDetail
    {
        [Key]
        [Column("ID_Inventaire_Detail")]
        public long IdInventaireDetail { get; set; }
        [Column("ID_Inventaire")]
        public long IdInventaire { get; set; }
        [Column("ID_Element")]
        public int IdElement { get; set; }
        [Column("Quantite_Casier_Theorique")]
        public int QuantiteCasierTheorique { get; set; }
        [Column("Quantite_Plastique_Theorique")]
        public int QuantitePlastiqueTheorique { get; set; }
        [Column("Quantite_Bouteille_Theorique")]
        public int QuantiteBouteilleTheorique { get; set; }
        [Column("Quantite_Casier_Reel")]
        public int QuantiteCasierReel { get; set; }
        [Column("Quantite_Plastique_Reel")]
        public int QuantitePlastiqueReel { get; set; }
        [Column("Quantite_Bouteille_Reel")]
        public int QuantiteBouteilleReel { get; set; }
        [Column("PU_Casier_Bouteille")]
        public double PuCasierBouteille { get; set; }
        [Column("PU_Plastique")]
        public double PuPlastique { get; set; }
        public int Conditionnement { get; set; }

        [ForeignKey("IdInventaire")]
        [InverseProperty("TpInventaireDetail")]
        public TpInventaire IdInventaireNavigation { get; set; }
    }
}
