﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Retour_Emballage_Temp")]
    public partial class TpRetourEmballageTemp
    {
        [Key]
        [Column("ID_Retour_Emballage_Temp")]
        public long IdRetourEmballageTemp { get; set; }
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Column("PU_Plastique")]
        public double PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double PuBouteille { get; set; }
        public int Conditionnement { get; set; }
        [Column("Quantite_Casier")]
        public int QuantiteCasier { get; set; }
        [Column("Quantite_Plastique")]
        public int QuantitePlastique { get; set; }
        [Column("Quantite_Bouteille")]
        public int QuantiteBouteille { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }

        [ForeignKey("IdEmballage")]
        [InverseProperty("TpRetourEmballageTemp")]
        public TpEmballage IdEmballageNavigation { get; set; }
    }
}
