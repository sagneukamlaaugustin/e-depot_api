﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Vente_Liquide")]
    public partial class TpVenteLiquide
    {
        [Key]
        [Column("ID_Vente_Liquide")]
        public long IdVenteLiquide { get; set; }
        [Column("ID_Vente")]
        public long IdVente { get; set; }
        [Column("ID_Produit")]
        public int IdProduit { get; set; }
        [Column("PU_Vente")]
        public double? PuVente { get; set; }
        [Column("PU_Achat")]
        public double? PuAchat { get; set; }
        public int? Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double? PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double? PuBouteille { get; set; }
        [Column("Quantite_Casier_Liquide")]
        public int QuantiteCasierLiquide { get; set; }
        [Column("Quantite_Bouteille_Liquide")]
        public int QuantiteBouteilleLiquide { get; set; }
        [Column("Quantite_Plastique_Emballage")]
        public int QuantitePlastiqueEmballage { get; set; }
        [Column("Quantite_Bouteille_Emballage")]
        public int QuantiteBouteilleEmballage { get; set; }
        [Column("Quantite_Casier_Emballage")]
        public int QuantiteCasierEmballage { get; set; }
        [Column("Ristourne_Client")]
        public int? RistourneClient { get; set; }
        [Column("Produit_Decomposer")]
        public bool? ProduitDecomposer { get; set; }
        [Column("Cumuler_Ristourne")]
        public bool? CumulerRistourne { get; set; }

        [ForeignKey("IdProduit")]
        [InverseProperty("TpVenteLiquide")]
        public TpProduit IdProduitNavigation { get; set; }
        [ForeignKey("IdVente")]
        [InverseProperty("TpVenteLiquide")]
        public TpVente IdVenteNavigation { get; set; }
    }
}
