﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Decaissement")]
    public partial class TpDecaissement
    {
        [Key]
        [Column("ID_Decaissement")]
        public long IdDecaissement { get; set; }
        [Column("ID_Caisse")]
        public int IdCaisse { get; set; }
        [Column("Type_Element")]
        public byte TypeElement { get; set; }
        [Column("ID_Element")]
        public int IdElement { get; set; }
        [Column("NUM_DOC")]
        [StringLength(50)]
        public string NumDoc { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public double Montant { get; set; }
        [StringLength(500)]
        public string Motif { get; set; }
        public byte Etat { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }

        [ForeignKey("IdCaisse")]
        [InverseProperty("TpDecaissement")]
        public TpCaisse IdCaisseNavigation { get; set; }
    }
}
