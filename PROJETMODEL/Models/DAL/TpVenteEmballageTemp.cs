﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Vente_Emballage_Temp")]
    public partial class TpVenteEmballageTemp
    {
        [Key]
        [Column("ID_Vente_Emballage_Temp")]
        public long IdVenteEmballageTemp { get; set; }
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Column("Quantite_Casier")]
        public int QuantiteCasier { get; set; }
        [Column("Quantite_Plastique")]
        public int QuantitePlastique { get; set; }
        [Column("Quantite_Bouteille")]
        public int QuantiteBouteille { get; set; }
        public int Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double PuBouteille { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }

        [ForeignKey("IdEmballage")]
        [InverseProperty("TpVenteEmballageTemp")]
        public TpEmballage IdEmballageNavigation { get; set; }
    }
}
