﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Caisse")]
    public partial class TpCaisse
    {
        public TpCaisse()
        {
            TgPersonnels = new HashSet<TgPersonnels>();
            TpDecaissement = new HashSet<TpDecaissement>();
            TpEncaissement = new HashSet<TpEncaissement>();
            TpOuvertureClotureCaisse = new HashSet<TpOuvertureClotureCaisse>();
            TpVente = new HashSet<TpVente>();
        }

        [Key]
        [Column("ID_Caisse")]
        public int IdCaisse { get; set; }
        [Required]
        [StringLength(150)]
        public string Libelle { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [InverseProperty("IdCaisseNavigation")]
        public ICollection<TgPersonnels> TgPersonnels { get; set; }
        [InverseProperty("IdCaisseNavigation")]
        public ICollection<TpDecaissement> TpDecaissement { get; set; }
        [InverseProperty("IdCaissseNavigation")]
        public ICollection<TpEncaissement> TpEncaissement { get; set; }
        [InverseProperty("IdCaisseNavigation")]
        public ICollection<TpOuvertureClotureCaisse> TpOuvertureClotureCaisse { get; set; }
        [InverseProperty("IdCaisseNavigation")]
        public ICollection<TpVente> TpVente { get; set; }
    }
}
