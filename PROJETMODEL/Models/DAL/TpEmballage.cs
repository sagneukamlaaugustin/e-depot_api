﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Emballage")]
    public partial class TpEmballage
    {
        public TpEmballage()
        {
            TpCommandeEmballage = new HashSet<TpCommandeEmballage>();
            TpCommandeEmballageTemp = new HashSet<TpCommandeEmballageTemp>();
            TpProduitIdBouteilleBaseNavigation = new HashSet<TpProduit>();
            TpProduitIdEmballageNavigation = new HashSet<TpProduit>();
            TpProduitIdPlastiqueBaseNavigation = new HashSet<TpProduit>();
            TpRetourEmballageDetail = new HashSet<TpRetourEmballageDetail>();
            TpRetourEmballageTemp = new HashSet<TpRetourEmballageTemp>();
            TpVenteEmballage = new HashSet<TpVenteEmballage>();
            TpVenteEmballageTemp = new HashSet<TpVenteEmballageTemp>();
        }

        [Key]
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Required]
        [StringLength(50)]
        public string Code { get; set; }
        [Required]
        [StringLength(150)]
        public string Description { get; set; }
        public int Conditionnement { get; set; }
        [Column("PU_Plastique")]
        public double PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double PuBouteille { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }
        [Required]
        public bool? Actif { get; set; }

        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpCommandeEmballage> TpCommandeEmballage { get; set; }
        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpCommandeEmballageTemp> TpCommandeEmballageTemp { get; set; }
        [InverseProperty("IdBouteilleBaseNavigation")]
        public ICollection<TpProduit> TpProduitIdBouteilleBaseNavigation { get; set; }
        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpProduit> TpProduitIdEmballageNavigation { get; set; }
        [InverseProperty("IdPlastiqueBaseNavigation")]
        public ICollection<TpProduit> TpProduitIdPlastiqueBaseNavigation { get; set; }
        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpRetourEmballageDetail> TpRetourEmballageDetail { get; set; }
        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpRetourEmballageTemp> TpRetourEmballageTemp { get; set; }
        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpVenteEmballage> TpVenteEmballage { get; set; }
        [InverseProperty("IdEmballageNavigation")]
        public ICollection<TpVenteEmballageTemp> TpVenteEmballageTemp { get; set; }
    }
}
