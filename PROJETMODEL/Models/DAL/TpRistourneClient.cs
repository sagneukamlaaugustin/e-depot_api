﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Ristourne_Client")]
    public partial class TpRistourneClient
    {
        [Key]
        [Column("ID_Ristourne_Client")]
        public int IdRistourneClient { get; set; }
        [Column("ID_Client")]
        public int IdClient { get; set; }
        [Column("ID_Produit")]
        public int IdProduit { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Date { get; set; }
        public double Ristourne { get; set; }
        [Column("Cumuler_Ristourne")]
        public bool CumulerRistourne { get; set; }
        public bool Actif { get; set; }
        [Column("Create_Date", TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
        [Column("Create_Code_User")]
        public int CreateCodeUser { get; set; }
        [Column("Edit_Date", TypeName = "datetime")]
        public DateTime EditDate { get; set; }
        [Column("Edit_Code_User")]
        public int EditCodeUser { get; set; }

        [ForeignKey("IdClient")]
        [InverseProperty("TpRistourneClient")]
        public TpClient IdClientNavigation { get; set; }
        [ForeignKey("IdProduit")]
        [InverseProperty("TpRistourneClient")]
        public TpProduit IdProduitNavigation { get; set; }
    }
}
