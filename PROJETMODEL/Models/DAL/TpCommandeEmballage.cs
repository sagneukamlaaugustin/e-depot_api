﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PROJETMODEL.Models.DAL
{
    [Table("TP_Commande_Emballage")]
    public partial class TpCommandeEmballage
    {
        [Key]
        [Column("ID_Commande_Emballage")]
        public long IdCommandeEmballage { get; set; }
        [Column("ID_Commande")]
        public long IdCommande { get; set; }
        [Column("ID_Emballage")]
        public int IdEmballage { get; set; }
        [Column("PU_Plastique")]
        public double? PuPlastique { get; set; }
        [Column("PU_Bouteille")]
        public double? PuBouteille { get; set; }
        public int? Conditionnement { get; set; }
        [Column("Quantite_Casier")]
        public int QuantiteCasier { get; set; }
        [Column("Quantite_Plastique")]
        public int QuantitePlastique { get; set; }
        [Column("Quantite_Bouteille")]
        public int QuantiteBouteille { get; set; }

        [ForeignKey("IdCommande")]
        [InverseProperty("TpCommandeEmballage")]
        public TpCommande IdCommandeNavigation { get; set; }
        [ForeignKey("IdEmballage")]
        [InverseProperty("TpCommandeEmballage")]
        public TpEmballage IdEmballageNavigation { get; set; }
    }
}
