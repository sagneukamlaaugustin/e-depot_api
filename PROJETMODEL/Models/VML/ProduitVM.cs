﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PROJETMODEL.Models.VML
{
    public class ProduitVM
    {
        public int IdProduit { get; set; }
        public string Produit { get; set; }
        public int IdFamille { get; set; }
        public string Famille { get; set; }
        public int IdTypeProduit { get; set; }
        public string TypeProduit { get; set; }
        public string Code { get; set; }
        public int Contionnenement { get; set; }
        public double PrixVente => PrixVenteGros / Contionnenement;
        public double PrixVenteGros { get; set; }
        public byte[] Image { get; set; }
    }
}
