﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OData.Edm;
using NJsonSchema;
using NSwag.AspNetCore;
using PROJETMODEL.Models.DAL;

namespace PROJETMODEL
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Sql serveur connection
            var ConnectionString = Configuration["DbConnectionString:SnackDB"];
            services.AddDbContext<Store>(opts => opts.UseSqlServer(ConnectionString));

            //Nswag service
            services.AddSwagger();

            //Nswag Odata
            services.AddOData();

            // Mvc core configuration
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddCors();

            //services.AddMvcCore()
            //   .AddAuthorization()
            //   .AddJsonFormatters();

            // configure identity serveur
            services.AddAuthentication("Bearer")
           .AddIdentityServerAuthentication(options =>
           {
               options.Authority = Configuration["IdentityServeurPath"];
               options.RequireHttpsMetadata = false;

               options.ApiName = "PROJETMODEL";
           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseAuthentication();

            //Mvc configuration
            app.UseHttpsRedirection();
            //app.UseMvcWithDefaultRoute();
            app.UseMvc(b =>
             {
                 b.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
                 b.MapODataServiceRoute("odata", null, GetEdmModel());
             });


            // NSwag
            app.UseSwaggerUi3WithApiExplorer(settings =>
              {
                settings.GeneratorSettings.DefaultPropertyNameHandling =
                    PropertyNameHandling.CamelCase;

                settings.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Sale Snack API";
                    document.Info.Description = "L'API premet la vente de la boisson depuis les temunaux mobils";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.SwaggerContact
                    {
                        Name = "Tanos SAS",
                        Email = "sagneukamlaaugustin@gmail.com",
                        Url = "https://www.google.com"
                    };
                    document.Info.License = new NSwag.SwaggerLicense
                    {
                        Name = "Tanos SAS",
                        Url = ""
                    };
                };
            });
        }

        // Odata
        private static IEdmModel GetEdmModel()
        {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<TpProduit>("Produit");
            builder.EntitySet<TpVente>("Vente");
            builder.EntitySet<TpVenteLiquide>("VenteDetails");
            builder.EntitySet<TgUtilisateurs>("User");
            builder.EntitySet<TpFamille>("Famille");
            builder.EntitySet<TpCategorie>("Categorie");
            return builder.GetEdmModel();
        }
    }
}
