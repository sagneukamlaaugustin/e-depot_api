﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJETMODEL.Migrations
{
    public partial class InitialDataBase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Produit_Mobile",
                columns: table => new
                {
                    ID_Produit = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Prix_Snack = table.Column<double>(nullable: false),
                    Code = table.Column<string>(maxLength: 50, nullable: false),
                    Nom = table.Column<string>(maxLength: 50, nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produit_Mobile", x => x.ID_Produit);
                });

            migrationBuilder.CreateTable(
                name: "TG_Commandes",
                columns: table => new
                {
                    Code_Commande = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code_Commande_Parent = table.Column<int>(nullable: false),
                    Libelle = table.Column<string>(maxLength: 50, nullable: false),
                    ID_Control = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Ordre = table.Column<int>(nullable: false),
                    url = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Commandes", x => x.Code_Commande);
                });

            migrationBuilder.CreateTable(
                name: "TG_Corbeille",
                columns: table => new
                {
                    ID_Corbeille = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Table = table.Column<int>(nullable: false),
                    ID_Element = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Corbeille", x => x.ID_Corbeille);
                });

            migrationBuilder.CreateTable(
                name: "TG_Locked_Records",
                columns: table => new
                {
                    ID_Locked = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Table_Name = table.Column<string>(maxLength: 50, nullable: false),
                    Record_ID = table.Column<int>(nullable: false),
                    Computer_Name = table.Column<string>(maxLength: 50, nullable: false),
                    Lock_Date_Time = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Locked_Records", x => x.ID_Locked);
                });

            migrationBuilder.CreateTable(
                name: "TG_Logs",
                columns: table => new
                {
                    Id_Error = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Error_value = table.Column<string>(nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Code_user = table.Column<int>(nullable: false),
                    Nom_Poste_Travail = table.Column<string>(maxLength: 50, nullable: false),
                    Fonction = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Logs", x => x.Id_Error);
                });

            migrationBuilder.CreateTable(
                name: "TG_Profiles",
                columns: table => new
                {
                    Code_Profile = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Libelle = table.Column<string>(maxLength: 50, nullable: false),
                    Display = table.Column<bool>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Profiles", x => x.Code_Profile);
                });

            migrationBuilder.CreateTable(
                name: "TG_Sentinelles",
                columns: table => new
                {
                    ID_Sentinelle = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Code_User = table.Column<int>(nullable: false),
                    Action = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Sentinelles", x => x.ID_Sentinelle);
                });

            migrationBuilder.CreateTable(
                name: "TG_Sentinelles_Options",
                columns: table => new
                {
                    ID_Sentinelle = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sentinelle = table.Column<bool>(nullable: false),
                    SentinelleConnexion = table.Column<bool>(nullable: false),
                    SentinelleEndConnexion = table.Column<bool>(nullable: false),
                    SentinelleInsert = table.Column<bool>(nullable: false),
                    SentinelleEdit = table.Column<bool>(nullable: false),
                    SentinelleDelete = table.Column<bool>(nullable: false),
                    SentinellePrint = table.Column<bool>(nullable: false),
                    SentinelleLife = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Sentinelles_Options", x => x.ID_Sentinelle);
                });

            migrationBuilder.CreateTable(
                name: "TG_Variable",
                columns: table => new
                {
                    ID_Variable = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nom_Societe = table.Column<string>(maxLength: 150, nullable: false),
                    Numero_Contribuable = table.Column<string>(maxLength: 50, nullable: true),
                    Registre_Commerce = table.Column<string>(maxLength: 50, nullable: true),
                    Telephone = table.Column<string>(maxLength: 50, nullable: true),
                    Adresse = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 150, nullable: true),
                    Ville = table.Column<string>(maxLength: 50, nullable: true),
                    Logo = table.Column<byte[]>(nullable: true),
                    Appliquer_TVA = table.Column<bool>(nullable: true),
                    TVA = table.Column<double>(nullable: true),
                    Gestion_Emballage = table.Column<bool>(nullable: true),
                    Verifier_Etat_Stock = table.Column<bool>(nullable: true),
                    Cle0 = table.Column<string>(maxLength: 100, nullable: true),
                    Cle1 = table.Column<byte[]>(maxLength: 100, nullable: true),
                    Cle2 = table.Column<byte[]>(maxLength: 100, nullable: true),
                    Dossier_Sauvegarde = table.Column<string>(maxLength: 150, nullable: true),
                    Password_Sauvegarde = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Variable", x => x.ID_Variable);
                });

            migrationBuilder.CreateTable(
                name: "TP_Caisse",
                columns: table => new
                {
                    ID_Caisse = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Libelle = table.Column<string>(maxLength: 150, nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Caisse", x => x.ID_Caisse);
                });

            migrationBuilder.CreateTable(
                name: "TP_Client",
                columns: table => new
                {
                    ID_Client = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nom = table.Column<string>(maxLength: 150, nullable: false),
                    Adresse = table.Column<string>(maxLength: 50, nullable: true),
                    Telephone = table.Column<string>(maxLength: 50, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Client", x => x.ID_Client);
                });

            migrationBuilder.CreateTable(
                name: "TP_Emballage",
                columns: table => new
                {
                    ID_Emballage = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 150, nullable: false),
                    Conditionnement = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: false),
                    PU_Bouteille = table.Column<double>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Emballage", x => x.ID_Emballage);
                });

            migrationBuilder.CreateTable(
                name: "TP_Famille",
                columns: table => new
                {
                    ID_Famille = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Libelle = table.Column<string>(maxLength: 50, nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Famille", x => x.ID_Famille);
                });

            migrationBuilder.CreateTable(
                name: "TP_Fournisseur",
                columns: table => new
                {
                    ID_Fournisseur = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nom = table.Column<string>(maxLength: 150, nullable: false),
                    Adresse = table.Column<string>(maxLength: 50, nullable: true),
                    Telephone = table.Column<string>(maxLength: 50, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Fournisseur", x => x.ID_Fournisseur);
                });

            migrationBuilder.CreateTable(
                name: "TP_Inventaire",
                columns: table => new
                {
                    ID_Inventaire = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<byte>(nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Date_Debut = table.Column<DateTime>(type: "datetime", nullable: false),
                    Date_Fin = table.Column<DateTime>(type: "datetime", nullable: true),
                    Etat = table.Column<byte>(nullable: false),
                    Motif = table.Column<string>(maxLength: 500, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Inventaire", x => x.ID_Inventaire);
                });

            migrationBuilder.CreateTable(
                name: "TP_Regulation_Solde",
                columns: table => new
                {
                    ID_Regulation_Solde = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Element = table.Column<int>(nullable: false),
                    Type_Element = table.Column<byte>(nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Montant = table.Column<double>(nullable: false),
                    Motif = table.Column<string>(maxLength: 500, nullable: true),
                    Etat = table.Column<byte>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Regulation_Solde", x => x.ID_Regulation_Solde);
                });

            migrationBuilder.CreateTable(
                name: "TP_Regulation_Stock",
                columns: table => new
                {
                    ID_Regulation_Stock = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Element = table.Column<int>(nullable: false),
                    Type_Element = table.Column<byte>(nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false),
                    Motif = table.Column<string>(maxLength: 500, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Etat = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Regulation_Stock", x => x.ID_Regulation_Stock);
                });

            migrationBuilder.CreateTable(
                name: "TP_Retour_Emballage",
                columns: table => new
                {
                    ID_Retour_Emballage = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type_Element = table.Column<byte>(nullable: false),
                    ID_Element = table.Column<int>(nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Etat = table.Column<byte>(nullable: false),
                    Motif = table.Column<string>(maxLength: 500, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Retour_Emballage", x => x.ID_Retour_Emballage);
                });

            migrationBuilder.CreateTable(
                name: "TP_Type_Depense",
                columns: table => new
                {
                    ID_Type_Depense = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Libelle = table.Column<string>(maxLength: 50, nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Type_Depense", x => x.ID_Type_Depense);
                });

            migrationBuilder.CreateTable(
                name: "TG_Hierarchie_Commande",
                columns: table => new
                {
                    ID_Hierarchie = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code_Commande = table.Column<int>(nullable: false),
                    Code_Commande_Parent = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Hierarchie_Commande", x => x.ID_Hierarchie);
                    table.ForeignKey(
                        name: "FK_TG_Hierarchie_Commande_TG_Commandes1",
                        column: x => x.Code_Commande_Parent,
                        principalTable: "TG_Commandes",
                        principalColumn: "Code_Commande",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TG_Droits",
                columns: table => new
                {
                    ID_Droit = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code_Profile = table.Column<int>(nullable: false),
                    Code_Commande = table.Column<int>(nullable: false),
                    Executer = table.Column<bool>(nullable: false),
                    Disponible = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Droits", x => x.ID_Droit);
                    table.ForeignKey(
                        name: "TG_Commandes_TG_Droits_FK1",
                        column: x => x.Code_Commande,
                        principalTable: "TG_Commandes",
                        principalColumn: "Code_Commande",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "TG_Profiles_TG_Droits_FK1",
                        column: x => x.Code_Profile,
                        principalTable: "TG_Profiles",
                        principalColumn: "Code_Profile",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TG_Personnels",
                columns: table => new
                {
                    ID_Personnel = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Caisse = table.Column<int>(nullable: false),
                    Nom = table.Column<string>(maxLength: 150, nullable: false),
                    Prenom = table.Column<string>(maxLength: 150, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    Display = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Personnels", x => x.ID_Personnel);
                    table.ForeignKey(
                        name: "FK_TG_Personnels_TP_Caisse",
                        column: x => x.ID_Caisse,
                        principalTable: "TP_Caisse",
                        principalColumn: "ID_Caisse",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Decaissement",
                columns: table => new
                {
                    ID_Decaissement = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Caisse = table.Column<int>(nullable: false),
                    Type_Element = table.Column<byte>(nullable: false),
                    ID_Element = table.Column<int>(nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Montant = table.Column<double>(nullable: false),
                    Motif = table.Column<string>(maxLength: 500, nullable: true),
                    Etat = table.Column<byte>(nullable: false, defaultValueSql: "((1))"),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Decaissement", x => x.ID_Decaissement);
                    table.ForeignKey(
                        name: "FK_TP_Decaissement_TP_Caisse",
                        column: x => x.ID_Caisse,
                        principalTable: "TP_Caisse",
                        principalColumn: "ID_Caisse",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Encaissement",
                columns: table => new
                {
                    ID_Encaissement = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Client = table.Column<int>(nullable: false),
                    ID_Caissse = table.Column<int>(nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Montant = table.Column<double>(nullable: false),
                    Motif = table.Column<string>(maxLength: 500, nullable: true),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Etat = table.Column<byte>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Encaissement", x => x.ID_Encaissement);
                    table.ForeignKey(
                        name: "FK_TP_Encaissement_TP_Caisse",
                        column: x => x.ID_Caissse,
                        principalTable: "TP_Caisse",
                        principalColumn: "ID_Caisse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Encaissement_TP_Client",
                        column: x => x.ID_Client,
                        principalTable: "TP_Client",
                        principalColumn: "ID_Client",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Vente",
                columns: table => new
                {
                    ID_Vente = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Client = table.Column<int>(nullable: false),
                    ID_Caisse = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Nom_Client = table.Column<string>(maxLength: 150, nullable: false),
                    Montant_Consigne_Emballage = table.Column<double>(nullable: false),
                    Montant = table.Column<double>(nullable: false),
                    Solde_Liquide = table.Column<double>(nullable: true),
                    Solde_Emballage = table.Column<double>(nullable: true),
                    Etat = table.Column<byte>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Vente", x => x.ID_Vente);
                    table.ForeignKey(
                        name: "FK_TP_Vente_TP_Caisse",
                        column: x => x.ID_Caisse,
                        principalTable: "TP_Caisse",
                        principalColumn: "ID_Caisse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Vente_TP_Client",
                        column: x => x.ID_Client,
                        principalTable: "TP_Client",
                        principalColumn: "ID_Client",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Commande_Emballage_Temp",
                columns: table => new
                {
                    ID_Commande_Emballage_Temp = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Emballage = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: true),
                    PU_Bouteille = table.Column<double>(nullable: true),
                    Conditionnement = table.Column<int>(nullable: true),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Commande_Emballage_Temp", x => x.ID_Commande_Emballage_Temp);
                    table.ForeignKey(
                        name: "FK_TP_Commande_Emballage_Temp_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Retour_Emballage_Temp",
                columns: table => new
                {
                    ID_Retour_Emballage_Temp = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Emballage = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: false),
                    PU_Bouteille = table.Column<double>(nullable: false),
                    Conditionnement = table.Column<int>(nullable: false),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Retour_Emballage_Temp", x => x.ID_Retour_Emballage_Temp);
                    table.ForeignKey(
                        name: "FK_TP_Retour_Emballage_Fournisseur_Temp_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Vente_Emballage_Temp",
                columns: table => new
                {
                    ID_Vente_Emballage_Temp = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Emballage = table.Column<int>(nullable: false),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false),
                    Conditionnement = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: false),
                    PU_Bouteille = table.Column<double>(nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Vente_Emballage_Temp", x => x.ID_Vente_Emballage_Temp);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Emballage_Temp_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Produit",
                columns: table => new
                {
                    ID_Produit = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Produit_Base = table.Column<int>(nullable: true),
                    ID_Famille = table.Column<int>(nullable: false),
                    ID_Emballage = table.Column<int>(nullable: false),
                    Code = table.Column<string>(maxLength: 50, nullable: true),
                    Libelle = table.Column<string>(maxLength: 150, nullable: false),
                    PU_Achat = table.Column<double>(nullable: false),
                    PU_Vente = table.Column<double>(nullable: false),
                    Valeur_Emballage = table.Column<bool>(nullable: false),
                    Produit_Decomposer = table.Column<bool>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    ID_Plastique_Base = table.Column<int>(nullable: true),
                    ID_Bouteille_Base = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Produit", x => x.ID_Produit);
                    table.ForeignKey(
                        name: "FK_TP_Produit_TP_Emballage2",
                        column: x => x.ID_Bouteille_Base,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Produit_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Produit_TP_Producteur",
                        column: x => x.ID_Famille,
                        principalTable: "TP_Famille",
                        principalColumn: "ID_Famille",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Produit_TP_Emballage1",
                        column: x => x.ID_Plastique_Base,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Produit_TP_Produit",
                        column: x => x.ID_Produit_Base,
                        principalTable: "TP_Produit",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Commande",
                columns: table => new
                {
                    ID_Commande = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Fournisseur = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Montant = table.Column<double>(nullable: true),
                    NUM_DOC = table.Column<string>(maxLength: 50, nullable: true),
                    Etat = table.Column<byte>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Commande", x => x.ID_Commande);
                    table.ForeignKey(
                        name: "FK_TP_Commande_TP_Fournisseur",
                        column: x => x.ID_Fournisseur,
                        principalTable: "TP_Fournisseur",
                        principalColumn: "ID_Fournisseur",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Inventaire_Detail",
                columns: table => new
                {
                    ID_Inventaire_Detail = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Inventaire = table.Column<long>(nullable: false),
                    ID_Element = table.Column<int>(nullable: false),
                    Quantite_Casier_Theorique = table.Column<int>(nullable: false),
                    Quantite_Plastique_Theorique = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Theorique = table.Column<int>(nullable: false),
                    Quantite_Casier_Reel = table.Column<int>(nullable: false),
                    Quantite_Plastique_Reel = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Reel = table.Column<int>(nullable: false),
                    PU_Casier_Bouteille = table.Column<double>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: false),
                    Conditionnement = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Inventaire_Detail", x => x.ID_Inventaire_Detail);
                    table.ForeignKey(
                        name: "FK_TP_Inventaire_Detail_TP_Inventaire",
                        column: x => x.ID_Inventaire,
                        principalTable: "TP_Inventaire",
                        principalColumn: "ID_Inventaire",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Retour_Emballage_Detail",
                columns: table => new
                {
                    ID_Retour_Emballage_Detail = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Retour_Emballage = table.Column<long>(nullable: false),
                    ID_Emballage = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: false),
                    PU_Bouteille = table.Column<double>(nullable: false),
                    Conditionnement = table.Column<int>(nullable: false),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Retour_Emballage_Detail", x => x.ID_Retour_Emballage_Detail);
                    table.ForeignKey(
                        name: "FK_TP_Retour_Emballage_Detail_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Retour_Emballage_Detail_TP_Retour_Emballage",
                        column: x => x.ID_Retour_Emballage,
                        principalTable: "TP_Retour_Emballage",
                        principalColumn: "ID_Retour_Emballage",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TG_Utilisateurs",
                columns: table => new
                {
                    Code_User = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Personnel = table.Column<int>(nullable: false),
                    Login = table.Column<string>(maxLength: 50, nullable: false),
                    Somme_Ctrl = table.Column<string>(maxLength: 50, nullable: false),
                    Display = table.Column<bool>(nullable: false),
                    Last_Connexion = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Utilisateurs", x => x.Code_User);
                    table.ForeignKey(
                        name: "TG_Personnels_TG_Utilisateurs_FK1",
                        column: x => x.ID_Personnel,
                        principalTable: "TG_Personnels",
                        principalColumn: "ID_Personnel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Ouverture_Cloture_Caisse",
                columns: table => new
                {
                    ID_Ouverture_Cloture_Caisse = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Caisse = table.Column<int>(nullable: false),
                    Date_Ouverture = table.Column<DateTime>(type: "datetime", nullable: false),
                    Date_Cloture = table.Column<DateTime>(type: "datetime", nullable: true),
                    Solde_Caisse = table.Column<double>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Etat = table.Column<byte>(nullable: false, defaultValueSql: "((1))"),
                    ID_Personnel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Ouverture_Cloture_Caisse", x => x.ID_Ouverture_Cloture_Caisse);
                    table.ForeignKey(
                        name: "FK_TP_Ouverture_Cloture_Caisse_TP_Caisse",
                        column: x => x.ID_Caisse,
                        principalTable: "TP_Caisse",
                        principalColumn: "ID_Caisse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Ouverture_Cloture_Caisse_TG_Personnels",
                        column: x => x.ID_Personnel,
                        principalTable: "TG_Personnels",
                        principalColumn: "ID_Personnel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vente_Mobile",
                columns: table => new
                {
                    ID_Vente_Mobile = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Personnel = table.Column<int>(nullable: false),
                    Date_Operation = table.Column<DateTime>(type: "datetime", nullable: false),
                    Etat = table.Column<byte>(nullable: false),
                    Status = table.Column<bool>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vente_Mobile", x => x.ID_Vente_Mobile);
                    table.ForeignKey(
                        name: "FK_Vente_Mobile_TG_Personnels",
                        column: x => x.ID_Personnel,
                        principalTable: "TG_Personnels",
                        principalColumn: "ID_Personnel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Vente_Emballage",
                columns: table => new
                {
                    ID_Vente_Emballage = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Vente = table.Column<long>(nullable: false),
                    ID_Emballage = table.Column<int>(nullable: false),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false),
                    Conditionnement = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: false),
                    PU_Bouteille = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Vente_Emballage", x => x.ID_Vente_Emballage);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Emballage_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Emballage_TP_Vente",
                        column: x => x.ID_Vente,
                        principalTable: "TP_Vente",
                        principalColumn: "ID_Vente",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Commande_Liquide_Temp",
                columns: table => new
                {
                    ID_Commande_Liquide_Temp = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Produit = table.Column<int>(nullable: false),
                    PU_Achat = table.Column<double>(nullable: true),
                    Conditionnement = table.Column<int>(nullable: true),
                    PU_Plastique = table.Column<double>(nullable: true),
                    PU_Bouteille = table.Column<double>(nullable: true),
                    Quantite_Casier_Liquide = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Liquide = table.Column<int>(nullable: false),
                    Quantite_Casier_Emballage = table.Column<int>(nullable: false),
                    Quantite_Plastique_Emballage = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Emballage = table.Column<int>(nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Commande_Liquide_Temp", x => x.ID_Commande_Liquide_Temp);
                    table.ForeignKey(
                        name: "FK_TP_Commande_Liquide_Temp_TP_Produit",
                        column: x => x.ID_Produit,
                        principalTable: "TP_Produit",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Ristourne_Client",
                columns: table => new
                {
                    ID_Ristourne_Client = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Client = table.Column<int>(nullable: false),
                    ID_Produit = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Ristourne = table.Column<double>(nullable: false),
                    Cumuler_Ristourne = table.Column<bool>(nullable: false),
                    Actif = table.Column<bool>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Ristourne_Client", x => x.ID_Ristourne_Client);
                    table.ForeignKey(
                        name: "FK_TP_Ristourne_Client_TP_Client",
                        column: x => x.ID_Client,
                        principalTable: "TP_Client",
                        principalColumn: "ID_Client",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Ristourne_Client_TP_Produit",
                        column: x => x.ID_Produit,
                        principalTable: "TP_Produit",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Vente_Liquide",
                columns: table => new
                {
                    ID_Vente_Liquide = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Vente = table.Column<long>(nullable: false),
                    ID_Produit = table.Column<int>(nullable: false),
                    PU_Vente = table.Column<double>(nullable: true),
                    PU_Achat = table.Column<double>(nullable: true),
                    Conditionnement = table.Column<int>(nullable: true),
                    PU_Plastique = table.Column<double>(nullable: true),
                    PU_Bouteille = table.Column<double>(nullable: true),
                    Quantite_Casier_Liquide = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Liquide = table.Column<int>(nullable: false),
                    Quantite_Plastique_Emballage = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Emballage = table.Column<int>(nullable: false),
                    Quantite_Casier_Emballage = table.Column<int>(nullable: false),
                    Ristourne_Client = table.Column<int>(nullable: true),
                    Produit_Decomposer = table.Column<bool>(nullable: true),
                    Cumuler_Ristourne = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Vente_Liquide", x => x.ID_Vente_Liquide);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Liquide_TP_Produit",
                        column: x => x.ID_Produit,
                        principalTable: "TP_Produit",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Liquide_TP_Vente",
                        column: x => x.ID_Vente,
                        principalTable: "TP_Vente",
                        principalColumn: "ID_Vente",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Vente_Liquide_Temp",
                columns: table => new
                {
                    ID_Vente_Liquide_Temp = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Client = table.Column<int>(nullable: false),
                    ID_Produit = table.Column<int>(nullable: false),
                    PU_Vente = table.Column<double>(nullable: true),
                    PU_Achat = table.Column<double>(nullable: true),
                    Conditionnement = table.Column<int>(nullable: true),
                    PU_Plastique = table.Column<double>(nullable: true),
                    PU_Bouteille = table.Column<double>(nullable: true),
                    Quantite_Casier_Liquide = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Liquide = table.Column<int>(nullable: false),
                    Quantite_Casier_Emballage = table.Column<int>(nullable: false),
                    Quantite_Plastique_Emballage = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Emballage = table.Column<int>(nullable: false),
                    Ristourne_Client = table.Column<double>(nullable: true),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Produit_Decomposer = table.Column<bool>(nullable: true),
                    Cumuler_Ristourne = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Vente_Liquide_Temp", x => x.ID_Vente_Liquide_Temp);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Liquide_Temp_TP_Client",
                        column: x => x.ID_Client,
                        principalTable: "TP_Client",
                        principalColumn: "ID_Client",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Vente_Liquide_Temp_TP_Produit",
                        column: x => x.ID_Produit,
                        principalTable: "TP_Produit",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Commande_Emballage",
                columns: table => new
                {
                    ID_Commande_Emballage = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Commande = table.Column<long>(nullable: false),
                    ID_Emballage = table.Column<int>(nullable: false),
                    PU_Plastique = table.Column<double>(nullable: true),
                    PU_Bouteille = table.Column<double>(nullable: true),
                    Conditionnement = table.Column<int>(nullable: true),
                    Quantite_Casier = table.Column<int>(nullable: false),
                    Quantite_Plastique = table.Column<int>(nullable: false),
                    Quantite_Bouteille = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Commande_Emballage", x => x.ID_Commande_Emballage);
                    table.ForeignKey(
                        name: "FK_TP_Commande_Emballage_TP_Commande",
                        column: x => x.ID_Commande,
                        principalTable: "TP_Commande",
                        principalColumn: "ID_Commande",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Commande_Emballage_TP_Emballage",
                        column: x => x.ID_Emballage,
                        principalTable: "TP_Emballage",
                        principalColumn: "ID_Emballage",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TP_Commande_Liquide",
                columns: table => new
                {
                    ID_Commande_Liquide = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Commande = table.Column<long>(nullable: false),
                    ID_Produit = table.Column<int>(nullable: false),
                    PU_Achat = table.Column<double>(nullable: true),
                    Conditionnement = table.Column<int>(nullable: true),
                    PU_Plastique = table.Column<double>(nullable: true),
                    PU_Bouteille = table.Column<double>(nullable: true),
                    Quantite_Casier_Liquide = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Liquide = table.Column<int>(nullable: false),
                    Quantite_Casier_Emballage = table.Column<int>(nullable: false),
                    Quantite_Plastique_Emballage = table.Column<int>(nullable: false),
                    Quantite_Bouteille_Emballage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TP_Commande_Liquide", x => x.ID_Commande_Liquide);
                    table.ForeignKey(
                        name: "FK_TP_Commande_Liquide_TP_Commande",
                        column: x => x.ID_Commande,
                        principalTable: "TP_Commande",
                        principalColumn: "ID_Commande",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TP_Commande_Liquide_TP_Produit",
                        column: x => x.ID_Produit,
                        principalTable: "TP_Produit",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TG_Profiles_Utilisateurs",
                columns: table => new
                {
                    ID_Profiles_Utilisateur = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code_User = table.Column<int>(nullable: false),
                    Code_Profile = table.Column<int>(nullable: false),
                    Create_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Edit_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Create_Code_User = table.Column<int>(nullable: false),
                    Edit_Code_User = table.Column<int>(nullable: false),
                    Actif = table.Column<bool>(nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TG_Profiles_Utilisateurs", x => x.ID_Profiles_Utilisateur);
                    table.ForeignKey(
                        name: "TG_Profiles_TG_Profiles_Utilisateurs_FK1",
                        column: x => x.Code_Profile,
                        principalTable: "TG_Profiles",
                        principalColumn: "Code_Profile",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "TG_Utilisateurs_TG_Profiles_Utilisateurs_FK1",
                        column: x => x.Code_User,
                        principalTable: "TG_Utilisateurs",
                        principalColumn: "Code_User",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Vente_Mobile_Details",
                columns: table => new
                {
                    ID_Vente_Mobile_Details = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ID_Vente = table.Column<int>(nullable: false),
                    ID_Produit = table.Column<int>(nullable: false),
                    Prix_Snack = table.Column<double>(nullable: false),
                    Quantite = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vente_Mobile_Details", x => x.ID_Vente_Mobile_Details);
                    table.ForeignKey(
                        name: "FK_Vente_Mobile_Details_Produit_Mobile",
                        column: x => x.ID_Produit,
                        principalTable: "Produit_Mobile",
                        principalColumn: "ID_Produit",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Vente_Mobile_Details_Vente_Mobile",
                        column: x => x.ID_Vente,
                        principalTable: "Vente_Mobile",
                        principalColumn: "ID_Vente_Mobile",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TG_Droits_Code_Commande",
                table: "TG_Droits",
                column: "Code_Commande");

            migrationBuilder.CreateIndex(
                name: "IX_TG_Droits_Code_Profile",
                table: "TG_Droits",
                column: "Code_Profile");

            migrationBuilder.CreateIndex(
                name: "IX_TG_Hierarchie_Commande_Code_Commande_Parent",
                table: "TG_Hierarchie_Commande",
                column: "Code_Commande_Parent");

            migrationBuilder.CreateIndex(
                name: "IX_TG_Personnels_ID_Caisse",
                table: "TG_Personnels",
                column: "ID_Caisse");

            migrationBuilder.CreateIndex(
                name: "IX_TG_Profiles_Utilisateurs_Code_Profile",
                table: "TG_Profiles_Utilisateurs",
                column: "Code_Profile");

            migrationBuilder.CreateIndex(
                name: "IX_TG_Profiles_Utilisateurs_Code_User",
                table: "TG_Profiles_Utilisateurs",
                column: "Code_User");

            migrationBuilder.CreateIndex(
                name: "IX_TG_Utilisateurs_ID_Personnel",
                table: "TG_Utilisateurs",
                column: "ID_Personnel");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_ID_Fournisseur",
                table: "TP_Commande",
                column: "ID_Fournisseur");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_Emballage_ID_Commande",
                table: "TP_Commande_Emballage",
                column: "ID_Commande");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_Emballage_ID_Emballage",
                table: "TP_Commande_Emballage",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_Emballage_Temp_ID_Emballage",
                table: "TP_Commande_Emballage_Temp",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_Liquide_ID_Commande",
                table: "TP_Commande_Liquide",
                column: "ID_Commande");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_Liquide_ID_Produit",
                table: "TP_Commande_Liquide",
                column: "ID_Produit");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Commande_Liquide_Temp_ID_Produit",
                table: "TP_Commande_Liquide_Temp",
                column: "ID_Produit");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Decaissement_ID_Caisse",
                table: "TP_Decaissement",
                column: "ID_Caisse");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Encaissement_ID_Caissse",
                table: "TP_Encaissement",
                column: "ID_Caissse");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Encaissement_ID_Client",
                table: "TP_Encaissement",
                column: "ID_Client");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Inventaire_Detail_ID_Inventaire",
                table: "TP_Inventaire_Detail",
                column: "ID_Inventaire");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Ouverture_Cloture_Caisse_ID_Caisse",
                table: "TP_Ouverture_Cloture_Caisse",
                column: "ID_Caisse");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Ouverture_Cloture_Caisse_ID_Personnel",
                table: "TP_Ouverture_Cloture_Caisse",
                column: "ID_Personnel");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Produit_ID_Bouteille_Base",
                table: "TP_Produit",
                column: "ID_Bouteille_Base");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Produit_ID_Emballage",
                table: "TP_Produit",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Produit_ID_Famille",
                table: "TP_Produit",
                column: "ID_Famille");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Produit_ID_Plastique_Base",
                table: "TP_Produit",
                column: "ID_Plastique_Base");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Produit_ID_Produit_Base",
                table: "TP_Produit",
                column: "ID_Produit_Base");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Retour_Emballage_Detail_ID_Emballage",
                table: "TP_Retour_Emballage_Detail",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Retour_Emballage_Detail_ID_Retour_Emballage",
                table: "TP_Retour_Emballage_Detail",
                column: "ID_Retour_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Retour_Emballage_Temp_ID_Emballage",
                table: "TP_Retour_Emballage_Temp",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Ristourne_Client_ID_Client",
                table: "TP_Ristourne_Client",
                column: "ID_Client");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Ristourne_Client_ID_Produit",
                table: "TP_Ristourne_Client",
                column: "ID_Produit");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_ID_Caisse",
                table: "TP_Vente",
                column: "ID_Caisse");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_ID_Client",
                table: "TP_Vente",
                column: "ID_Client");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Emballage_ID_Emballage",
                table: "TP_Vente_Emballage",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Emballage_ID_Vente",
                table: "TP_Vente_Emballage",
                column: "ID_Vente");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Emballage_Temp_ID_Emballage",
                table: "TP_Vente_Emballage_Temp",
                column: "ID_Emballage");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Liquide_ID_Produit",
                table: "TP_Vente_Liquide",
                column: "ID_Produit");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Liquide_ID_Vente",
                table: "TP_Vente_Liquide",
                column: "ID_Vente");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Liquide_Temp_ID_Client",
                table: "TP_Vente_Liquide_Temp",
                column: "ID_Client");

            migrationBuilder.CreateIndex(
                name: "IX_TP_Vente_Liquide_Temp_ID_Produit",
                table: "TP_Vente_Liquide_Temp",
                column: "ID_Produit");

            migrationBuilder.CreateIndex(
                name: "IX_Vente_Mobile_ID_Personnel",
                table: "Vente_Mobile",
                column: "ID_Personnel");

            migrationBuilder.CreateIndex(
                name: "IX_Vente_Mobile_Details_ID_Produit",
                table: "Vente_Mobile_Details",
                column: "ID_Produit");

            migrationBuilder.CreateIndex(
                name: "IX_Vente_Mobile_Details_ID_Vente",
                table: "Vente_Mobile_Details",
                column: "ID_Vente");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TG_Corbeille");

            migrationBuilder.DropTable(
                name: "TG_Droits");

            migrationBuilder.DropTable(
                name: "TG_Hierarchie_Commande");

            migrationBuilder.DropTable(
                name: "TG_Locked_Records");

            migrationBuilder.DropTable(
                name: "TG_Logs");

            migrationBuilder.DropTable(
                name: "TG_Profiles_Utilisateurs");

            migrationBuilder.DropTable(
                name: "TG_Sentinelles");

            migrationBuilder.DropTable(
                name: "TG_Sentinelles_Options");

            migrationBuilder.DropTable(
                name: "TG_Variable");

            migrationBuilder.DropTable(
                name: "TP_Commande_Emballage");

            migrationBuilder.DropTable(
                name: "TP_Commande_Emballage_Temp");

            migrationBuilder.DropTable(
                name: "TP_Commande_Liquide");

            migrationBuilder.DropTable(
                name: "TP_Commande_Liquide_Temp");

            migrationBuilder.DropTable(
                name: "TP_Decaissement");

            migrationBuilder.DropTable(
                name: "TP_Encaissement");

            migrationBuilder.DropTable(
                name: "TP_Inventaire_Detail");

            migrationBuilder.DropTable(
                name: "TP_Ouverture_Cloture_Caisse");

            migrationBuilder.DropTable(
                name: "TP_Regulation_Solde");

            migrationBuilder.DropTable(
                name: "TP_Regulation_Stock");

            migrationBuilder.DropTable(
                name: "TP_Retour_Emballage_Detail");

            migrationBuilder.DropTable(
                name: "TP_Retour_Emballage_Temp");

            migrationBuilder.DropTable(
                name: "TP_Ristourne_Client");

            migrationBuilder.DropTable(
                name: "TP_Type_Depense");

            migrationBuilder.DropTable(
                name: "TP_Vente_Emballage");

            migrationBuilder.DropTable(
                name: "TP_Vente_Emballage_Temp");

            migrationBuilder.DropTable(
                name: "TP_Vente_Liquide");

            migrationBuilder.DropTable(
                name: "TP_Vente_Liquide_Temp");

            migrationBuilder.DropTable(
                name: "Vente_Mobile_Details");

            migrationBuilder.DropTable(
                name: "TG_Commandes");

            migrationBuilder.DropTable(
                name: "TG_Profiles");

            migrationBuilder.DropTable(
                name: "TG_Utilisateurs");

            migrationBuilder.DropTable(
                name: "TP_Commande");

            migrationBuilder.DropTable(
                name: "TP_Inventaire");

            migrationBuilder.DropTable(
                name: "TP_Retour_Emballage");

            migrationBuilder.DropTable(
                name: "TP_Vente");

            migrationBuilder.DropTable(
                name: "TP_Produit");

            migrationBuilder.DropTable(
                name: "Produit_Mobile");

            migrationBuilder.DropTable(
                name: "Vente_Mobile");

            migrationBuilder.DropTable(
                name: "TP_Fournisseur");

            migrationBuilder.DropTable(
                name: "TP_Client");

            migrationBuilder.DropTable(
                name: "TP_Emballage");

            migrationBuilder.DropTable(
                name: "TP_Famille");

            migrationBuilder.DropTable(
                name: "TG_Personnels");

            migrationBuilder.DropTable(
                name: "TP_Caisse");
        }
    }
}
